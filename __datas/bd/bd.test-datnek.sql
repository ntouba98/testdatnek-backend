﻿-- test-datnek --

-- table
DROP TABLE IF EXISTS test_datnek_lang;
CREATE TABLE IF NOT EXISTS test_datnek_lang (
	code VARCHAR(30) NOT NULL PRIMARY KEY,
	lang enum('en', 'fr', 'nl') NOT NULL UNIQUE,
	spoken_level enum('intermediate', 'pre_intermediate', 'current', 'elementary') NOT NULL,
	written_level enum('intermediate', 'pre_intermediate', 'current', 'elementary') NOT NULL,
	understanding_level enum('intermediate', 'pre_intermediate', 'current', 'elementary') NOT NULL,
	date_added DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
	date_updated DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    status ENUM( 'visible', 'archived', 'blocked' ) NOT NULL DEFAULT 'visible'
);

-- FUNCTIONS, PROCEDURES OR TRIGGERS


-- views
DROP VIEW IF EXISTS view_test_datnek_lang;
CREATE OR REPLACE VIEW view_test_datnek_lang AS
SELECT
    L.*
FROM test_datnek_lang L;