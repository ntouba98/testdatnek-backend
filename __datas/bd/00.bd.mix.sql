﻿-- other --

-- type
-- enum_classic AS ENUM( 'visible', 'archived', 'blocked' );
-- enum_sexe_classic AS ENUM( 'm', 'f' );
-- enum_media AS ENUM( 'audio', 'video', 'img' );
-- enum_file AS ENUM( 'audio', 'video', 'img', 'file', 'link' );
-- enum_social_network AS ENUM( 'facebook', 'twitter', 'instagram', 'youtube', 'lindkeldn' );
-- enum_direction AS ENUM( 'left', 'right', 'center' );
-- enum_time_type AS ENUM( 'second', 'minute', 'hour', 'day', 'month', 'year' );
-- enum_subscription AS ENUM( 'all' );

-- trigger, functions and procedure

/*
DROP FUNCTION IF EXISTS customFunct;
DELIMITER $$
CREATE FUNCTION customFunct(
)
RETURNS BOOLEAN
BEGIN
    DECLARE result BOOLEAN;

    RETURN result;
END
$$ DELIMITER ;
*/
DROP PROCEDURE IF EXISTS ROWS_TO_JSON;
DELIMITER $$
CREATE PROCEDURE ROWS_TO_JSON(
    columns JSON,
    tablename TEXT,
    supreq LONGTEXT,
    single BOOLEAN,
    OUT res JSON
)
BEGIN
	DECLARE i INTEGER DEFAULT 0;
	DECLARE j INTEGER DEFAULT 0;
    DECLARE DONE INTEGER DEFAULT 0;
    DECLARE row_length INTEGER DEFAULT 0;
	DECLARE rowRES_json JSON;
	DECLARE req_rowRES_json TEXT DEFAULT '';
	DECLARE req_columns TEXT DEFAULT '';
	DECLARE req_stmt TEXT DEFAULT '';
    DECLARE result JSON DEFAULT '[]';
    
    IF JSON_LENGTH(columns) > 0 THEN
    	SET columns = columns;
    -- ELSE
    END IF;
    
    
    SET i = 0;
    WHILE i < JSON_LENGTH(JSON_KEYS(columns)) DO
    	SET @column_name = JSON_EXTRACT(
            JSON_KEYS(columns)
            , CONCAT('$[', i, ']')
        );
    	SET @column_value = JSON_UNQUOTE(
            JSON_EXTRACT(
                JSON_UNQUOTE(
                	JSON_EXTRACT(columns, '$.*')
                ), CONCAT('$[', i, ']')
            )
        );
        SET req_columns = CONCAT( req_columns, @column_name, ' , ', @column_value );
        
        IF i < JSON_LENGTH(columns) - 1 THEN
            SET req_columns = CONCAT( req_columns, ', ' );
        END IF;
        
        SET i = i + 1;
    END WHILE;
    SET req_columns = JSON_UNQUOTE(
        CONCAT('JSON_OBJECT( ', req_columns, ' )')
    );
    -- SELECT req_columns;
    
    SET @req_stmt1 = CONCAT( 'SELECT COUNT(*) INTO @row_length FROM ', tablename , IF( LENGTH(supreq) > 0, CONCAT(' ', supreq), '' ));
    PREPARE stmt1 FROM @req_stmt1; 
    EXECUTE stmt1; 
    DEALLOCATE PREPARE stmt1;
    -- 
    SET @rows = '[]';
    SET i = 0;
    WHILE i < @row_length DO
        SET @row = '{}';
        SET j = 0;
        WHILE j < JSON_LENGTH(JSON_KEYS(columns)) DO
            SET @column_name = JSON_UNQUOTE(
                JSON_EXTRACT(
                    JSON_KEYS(columns)
                    , CONCAT('$[', j, ']')
                )
            );
            SET @column_value = JSON_UNQUOTE(
                JSON_EXTRACT(
                    JSON_UNQUOTE(
                        JSON_EXTRACT(columns, '$.*')
                    ), CONCAT('$[', j, ']')
                )
            );
            -- 
            SET @req_stmt2 = CONCAT( 'SELECT (', @column_value, ') AS ', @column_name, ' INTO @column_value FROM ', tablename , IF( LENGTH(supreq) > 0, CONCAT(' ', supreq), '' ), ' limit ', i, ', 1');
            PREPARE stmt2 FROM @req_stmt2; 
            EXECUTE stmt2; 
            DEALLOCATE PREPARE stmt2;
            -- 
            SET @row = JSON_MERGE(
                @row,
                JSON_OBJECT( @column_name, @column_value )
            );
            -- 
            SET j = j + 1;
        END WHILE;
        -- 
        SET @rows = JSON_MERGE(
        	@rows,
            @row
        );
        
        SET i = i + 1;
    END WHILE;
    
    
    SET res = IF(
        single = TRUE
        , JSON_UNQUOTE(
            JSON_EXTRACT(@rows, CONCAT('$[0]'))
        ) , @rows
    );
END
$$ DELIMITER ;
/*
CALL ROWS_TO_JSON(
    '{
    	"code": "T.coded"
    	, "name": "T.name"
    	, "parent_code": "T.parent_code"
    }'
    , 'tree_element_test T'
    , NULL
    , FALSE
    , @res
);
SELECT @res;
*/

DROP FUNCTION IF EXISTS getIntervalTime;
DELIMITER $$
CREATE FUNCTION getIntervalTime(
	duration REAL,
    timeType ENUM( 'second', 'minute', 'hour', 'day', 'month', 'year' )
)
RETURNS REAL
BEGIN
    DECLARE initialres DATETIME;
    DECLARE result REAL;
    DECLARE resultDATE DATETIME;
	DECLARE defaultDate DATETIME;

	SET defaultDate = STR_TO_DATE('01-01-0001 00:00:00', '%d-%m-%Y %H:%i:%s');

	IF timeType = 'second' THEN
        SET result = TIMESTAMPDIFF(
            SECOND
            , defaultDate
            , defaultDate + INTERVAL duration SECOND
        );
    ELSEIF timeType = 'minute' THEN
        SET result = TIMESTAMPDIFF(
            SECOND
            , defaultDate
            , defaultDate + INTERVAL duration MINUTE
        );
    ELSEIF timeType = 'hour' THEN
        SET result = TIMESTAMPDIFF(
            SECOND
            , defaultDate
            , defaultDate + INTERVAL duration HOUR
        );
    ELSEIF timeType = 'day' THEN
        SET result = TIMESTAMPDIFF(
            SECOND
            , defaultDate
            , defaultDate + INTERVAL duration DAY
        );
    ELSEIF timeType = 'month' THEN
        SET result = TIMESTAMPDIFF(
            SECOND
            , defaultDate
            , defaultDate + INTERVAL duration MONTH
        );
    ELSEIF timeType = 'year' THEN
        SET result = TIMESTAMPDIFF(
            SECOND
            , defaultDate
            , defaultDate + INTERVAL duration YEAR
        );
    ELSE
        SET result = TIMESTAMPDIFF(
            SECOND
            , defaultDate
            , defaultDate + INTERVAL duration SECOND
        );
    END IF;

    RETURN result;
END
$$ DELIMITER ;
-- SELECT getIntervalTime(1, 'month');
DROP FUNCTION IF EXISTS addIntervalInTime;
DELIMITER $$
CREATE FUNCTION addIntervalInTime(
	thedate DATETIME,
	duration REAL,
    timeType ENUM( 'second', 'minute', 'hour', 'day', 'month', 'year' )
)
RETURNS DATETIME
BEGIN
    DECLARE result DATETIME;
    IF timeType = 'second' THEN
    	SET result = thedate + INTERVAL duration SECOND;
    ELSEIF timeType = 'minute' THEN
    	SET result = thedate + INTERVAL duration MINUTE;
    ELSEIF timeType = 'hour' THEN
    	SET result = thedate + INTERVAL duration HOUR;
    ELSEIF timeType = 'day' THEN
    	SET result = thedate + INTERVAL duration DAY;
    ELSEIF timeType = 'month' THEN
    	SET result = thedate + INTERVAL duration MONTH;
    ELSEIF timeType = 'year' THEN
    	SET result = thedate + INTERVAL duration YEAR;
    ELSE
    	SET result = thedate + INTERVAL duration SECOND;
    END IF;

    RETURN result;
END
$$ DELIMITER ;
DROP FUNCTION IF EXISTS removeIntervalInTime;
DELIMITER $$
CREATE FUNCTION removeIntervalInTime(
	thedate DATETIME,
	duration REAL,
    timeType ENUM( 'second', 'minute', 'hour', 'day', 'month', 'year' )
)
RETURNS DATETIME
BEGIN
    DECLARE result DATETIME;
    IF timeType = 'second' THEN
    	SET result = thedate - INTERVAL duration SECOND;
    ELSEIF timeType = 'minute' THEN
    	SET result = thedate - INTERVAL duration MINUTE;
    ELSEIF timeType = 'hour' THEN
    	SET result = thedate - INTERVAL duration HOUR;
    ELSEIF timeType = 'day' THEN
    	SET result = thedate - INTERVAL duration DAY;
    ELSEIF timeType = 'month' THEN
    	SET result = thedate - INTERVAL duration MONTH;
    ELSEIF timeType = 'year' THEN
    	SET result = thedate - INTERVAL duration YEAR;
    ELSE
    	SET result = thedate - INTERVAL duration SECOND;
    END IF;

    RETURN result;
END
$$ DELIMITER ;
DROP FUNCTION IF EXISTS timeIsOver;
DELIMITER $$
CREATE FUNCTION timeIsOver(
	date1 DATETIME,
	date2 DATETIME,
	duration REAL,
    timeType ENUM( 'second', 'minute', 'hour', 'day', 'month', 'year' )
) 
RETURNS BOOLEAN
DETERMINISTIC
BEGIN
	-- declaration
    DECLARE date3 DATETIME;
    DECLARE res BOOLEAN;
    
    -- step2
	IF (ISNULL(date2) = 1) THEN
		SET date2 = date1;
	END IF;
	IF (ISNULL(date1) = 1) THEN
		SET date1 = date2;
	END IF;
    
    -- get date3
    IF timeType = 'second' THEN
    	SET date3 = date2 + INTERVAL duration SECOND;
    ELSEIF timeType = 'minute' THEN
    	SET date3 = date2 + INTERVAL duration MINUTE;
    ELSEIF timeType = 'hour' THEN
    	SET date3 = date2 + INTERVAL duration HOUR;
    ELSEIF timeType = 'day' THEN
    	SET date3 = date2 + INTERVAL duration DAY;
    ELSEIF timeType = 'month' THEN
    	SET date3 = date2 + INTERVAL duration MONTH;
    ELSEIF timeType = 'year' THEN
    	SET date3 = date2 + INTERVAL duration YEAR;
    ELSE
    	SET date3 = date2 + INTERVAL duration SECOND;
    END IF;
    
    -- get res
    SET res = (
        (
            date3 >= date1
        )
    );
    
	RETURN res;
END$$
DELIMITER ;
/* SELECT
    timeIsOver(
        '2007-12-31 10:02:00',
        '2007-12-30 12:01:01',
        80000,
        'second'
    ),
    TIMESTAMPDIFF(SECOND,'2007-12-30 12:01:01','2007-12-31 10:02:00')
; */
DROP FUNCTION IF EXISTS timeIsOverUseDatenow;
DELIMITER $$
CREATE FUNCTION timeIsOverUseDatenow(
	thedate DATETIME,
	duration REAL,
    timeType ENUM( 'second', 'minute', 'hour', 'day', 'month', 'year' )
)
RETURNS BOOLEAN
DETERMINISTIC
BEGIN
	RETURN timeIsOver(
        CURRENT_TIMESTAMP,
        thedate,
        duration,
        timetype
    );
END$$
DELIMITER ;
DROP FUNCTION IF EXISTS random_string;
DELIMITER $$
CREATE FUNCTION random_string(
    length INTEGER
)
RETURNS TEXT
DETERMINISTIC
BEGIN
	DECLARE chars JSON;
	DECLARE i INTEGER;
	DECLARE k INTEGER;
    DECLARE res TEXT;
    
    SET chars  = '[ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z" ]';
    SET res = '';
    SET i = 0;

    loop1: LOOP
        IF i < length THEN
        SET k = FLOOR( 0 + RAND() * (JSON_LENGTH(chars) - 1) );
        	SET res = CONCAT(
                res,
                JSON_EXTRACT(chars, CONCAT('$[', k, ']'))
            );
            SET i = i + 1;
        ELSE
            LEAVE loop1;
        END IF;
    END LOOP;
    SET res = REPLACE(res, '"', '');
    
    return res;
END
$$ DELIMITER ;
-- SELECT random_string(20);