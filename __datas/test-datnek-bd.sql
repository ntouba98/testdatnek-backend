-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3308
-- Généré le :  mer. 01 déc. 2021 à 22:39
-- Version du serveur :  5.7.24
-- Version de PHP :  7.2.14

SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `test-datnek-bd`
--

DELIMITER $$
--
-- Procédures
--
DROP PROCEDURE IF EXISTS `ROWS_TO_JSON`$$
CREATE DEFINER=`ntouba98`@`%` PROCEDURE `ROWS_TO_JSON` (`columns` JSON, `tablename` TEXT, `supreq` LONGTEXT, `single` BOOLEAN, OUT `res` JSON)  BEGIN
	DECLARE i INTEGER DEFAULT 0;
	DECLARE j INTEGER DEFAULT 0;
    DECLARE DONE INTEGER DEFAULT 0;
    DECLARE row_length INTEGER DEFAULT 0;
	DECLARE rowRES_json JSON;
	DECLARE req_rowRES_json TEXT DEFAULT '';
	DECLARE req_columns TEXT DEFAULT '';
	DECLARE req_stmt TEXT DEFAULT '';
    DECLARE result JSON DEFAULT '[]';
    
    IF JSON_LENGTH(columns) > 0 THEN
    	SET columns = columns;
    -- ELSE
    END IF;
    
    
    SET i = 0;
    WHILE i < JSON_LENGTH(JSON_KEYS(columns)) DO
    	SET @column_name = JSON_EXTRACT(
            JSON_KEYS(columns)
            , CONCAT('$[', i, ']')
        );
    	SET @column_value = JSON_UNQUOTE(
            JSON_EXTRACT(
                JSON_UNQUOTE(
                	JSON_EXTRACT(columns, '$.*')
                ), CONCAT('$[', i, ']')
            )
        );
        SET req_columns = CONCAT( req_columns, @column_name, ' , ', @column_value );
        
        IF i < JSON_LENGTH(columns) - 1 THEN
            SET req_columns = CONCAT( req_columns, ', ' );
        END IF;
        
        SET i = i + 1;
    END WHILE;
    SET req_columns = JSON_UNQUOTE(
        CONCAT('JSON_OBJECT( ', req_columns, ' )')
    );
    -- SELECT req_columns;
    
    SET @req_stmt1 = CONCAT( 'SELECT COUNT(*) INTO @row_length FROM ', tablename , IF( LENGTH(supreq) > 0, CONCAT(' ', supreq), '' ));
    PREPARE stmt1 FROM @req_stmt1; 
    EXECUTE stmt1; 
    DEALLOCATE PREPARE stmt1;
    -- 
    SET @rows = '[]';
    SET i = 0;
    WHILE i < @row_length DO
        SET @row = '{}';
        SET j = 0;
        WHILE j < JSON_LENGTH(JSON_KEYS(columns)) DO
            SET @column_name = JSON_UNQUOTE(
                JSON_EXTRACT(
                    JSON_KEYS(columns)
                    , CONCAT('$[', j, ']')
                )
            );
            SET @column_value = JSON_UNQUOTE(
                JSON_EXTRACT(
                    JSON_UNQUOTE(
                        JSON_EXTRACT(columns, '$.*')
                    ), CONCAT('$[', j, ']')
                )
            );
            -- 
            SET @req_stmt2 = CONCAT( 'SELECT (', @column_value, ') AS ', @column_name, ' INTO @column_value FROM ', tablename , IF( LENGTH(supreq) > 0, CONCAT(' ', supreq), '' ), ' limit ', i, ', 1');
            PREPARE stmt2 FROM @req_stmt2; 
            EXECUTE stmt2; 
            DEALLOCATE PREPARE stmt2;
            -- 
            SET @row = JSON_MERGE(
                @row,
                JSON_OBJECT( @column_name, @column_value )
            );
            -- 
            SET j = j + 1;
        END WHILE;
        -- 
        SET @rows = JSON_MERGE(
        	@rows,
            @row
        );
        
        SET i = i + 1;
    END WHILE;
    
    
    SET res = IF(
        single = TRUE
        , JSON_UNQUOTE(
            JSON_EXTRACT(@rows, CONCAT('$[0]'))
        ) , @rows
    );
END$$

--
-- Fonctions
--
DROP FUNCTION IF EXISTS `addIntervalInTime`$$
CREATE DEFINER=`ntouba98`@`%` FUNCTION `addIntervalInTime` (`thedate` DATETIME, `duration` REAL, `timeType` ENUM('second','minute','hour','day','month','year')) RETURNS DATETIME BEGIN
    DECLARE result DATETIME;
    IF timeType = 'second' THEN
    	SET result = thedate + INTERVAL duration SECOND;
    ELSEIF timeType = 'minute' THEN
    	SET result = thedate + INTERVAL duration MINUTE;
    ELSEIF timeType = 'hour' THEN
    	SET result = thedate + INTERVAL duration HOUR;
    ELSEIF timeType = 'day' THEN
    	SET result = thedate + INTERVAL duration DAY;
    ELSEIF timeType = 'month' THEN
    	SET result = thedate + INTERVAL duration MONTH;
    ELSEIF timeType = 'year' THEN
    	SET result = thedate + INTERVAL duration YEAR;
    ELSE
    	SET result = thedate + INTERVAL duration SECOND;
    END IF;

    RETURN result;
END$$

DROP FUNCTION IF EXISTS `getIntervalTime`$$
CREATE DEFINER=`ntouba98`@`%` FUNCTION `getIntervalTime` (`duration` REAL, `timeType` ENUM('second','minute','hour','day','month','year')) RETURNS DOUBLE BEGIN
    DECLARE initialres DATETIME;
    DECLARE result REAL;
    DECLARE resultDATE DATETIME;
	DECLARE defaultDate DATETIME;

	SET defaultDate = STR_TO_DATE('01-01-0001 00:00:00', '%d-%m-%Y %H:%i:%s');

	IF timeType = 'second' THEN
        SET result = TIMESTAMPDIFF(
            SECOND
            , defaultDate
            , defaultDate + INTERVAL duration SECOND
        );
    ELSEIF timeType = 'minute' THEN
        SET result = TIMESTAMPDIFF(
            SECOND
            , defaultDate
            , defaultDate + INTERVAL duration MINUTE
        );
    ELSEIF timeType = 'hour' THEN
        SET result = TIMESTAMPDIFF(
            SECOND
            , defaultDate
            , defaultDate + INTERVAL duration HOUR
        );
    ELSEIF timeType = 'day' THEN
        SET result = TIMESTAMPDIFF(
            SECOND
            , defaultDate
            , defaultDate + INTERVAL duration DAY
        );
    ELSEIF timeType = 'month' THEN
        SET result = TIMESTAMPDIFF(
            SECOND
            , defaultDate
            , defaultDate + INTERVAL duration MONTH
        );
    ELSEIF timeType = 'year' THEN
        SET result = TIMESTAMPDIFF(
            SECOND
            , defaultDate
            , defaultDate + INTERVAL duration YEAR
        );
    ELSE
        SET result = TIMESTAMPDIFF(
            SECOND
            , defaultDate
            , defaultDate + INTERVAL duration SECOND
        );
    END IF;

    RETURN result;
END$$

DROP FUNCTION IF EXISTS `random_string`$$
CREATE DEFINER=`ntouba98`@`%` FUNCTION `random_string` (`length` INTEGER) RETURNS TEXT CHARSET utf8 BEGIN
	DECLARE chars JSON;
	DECLARE i INTEGER;
	DECLARE k INTEGER;
    DECLARE res TEXT;
    
    SET chars  = '[ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z" ]';
    SET res = '';
    SET i = 0;

    loop1: LOOP
        IF i < length THEN
        SET k = FLOOR( 0 + RAND() * (JSON_LENGTH(chars) - 1) );
        	SET res = CONCAT(
                res,
                JSON_EXTRACT(chars, CONCAT('$[', k, ']'))
            );
            SET i = i + 1;
        ELSE
            LEAVE loop1;
        END IF;
    END LOOP;
    SET res = REPLACE(res, '"', '');
    
    return res;
END$$

DROP FUNCTION IF EXISTS `removeIntervalInTime`$$
CREATE DEFINER=`ntouba98`@`%` FUNCTION `removeIntervalInTime` (`thedate` DATETIME, `duration` REAL, `timeType` ENUM('second','minute','hour','day','month','year')) RETURNS DATETIME BEGIN
    DECLARE result DATETIME;
    IF timeType = 'second' THEN
    	SET result = thedate - INTERVAL duration SECOND;
    ELSEIF timeType = 'minute' THEN
    	SET result = thedate - INTERVAL duration MINUTE;
    ELSEIF timeType = 'hour' THEN
    	SET result = thedate - INTERVAL duration HOUR;
    ELSEIF timeType = 'day' THEN
    	SET result = thedate - INTERVAL duration DAY;
    ELSEIF timeType = 'month' THEN
    	SET result = thedate - INTERVAL duration MONTH;
    ELSEIF timeType = 'year' THEN
    	SET result = thedate - INTERVAL duration YEAR;
    ELSE
    	SET result = thedate - INTERVAL duration SECOND;
    END IF;

    RETURN result;
END$$

DROP FUNCTION IF EXISTS `timeIsOver`$$
CREATE DEFINER=`ntouba98`@`%` FUNCTION `timeIsOver` (`date1` DATETIME, `date2` DATETIME, `duration` REAL, `timeType` ENUM('second','minute','hour','day','month','year')) RETURNS TINYINT(1) BEGIN
	-- declaration
    DECLARE date3 DATETIME;
    DECLARE res BOOLEAN;
    
    -- step2
	IF (ISNULL(date2) = 1) THEN
		SET date2 = date1;
	END IF;
	IF (ISNULL(date1) = 1) THEN
		SET date1 = date2;
	END IF;
    
    -- get date3
    IF timeType = 'second' THEN
    	SET date3 = date2 + INTERVAL duration SECOND;
    ELSEIF timeType = 'minute' THEN
    	SET date3 = date2 + INTERVAL duration MINUTE;
    ELSEIF timeType = 'hour' THEN
    	SET date3 = date2 + INTERVAL duration HOUR;
    ELSEIF timeType = 'day' THEN
    	SET date3 = date2 + INTERVAL duration DAY;
    ELSEIF timeType = 'month' THEN
    	SET date3 = date2 + INTERVAL duration MONTH;
    ELSEIF timeType = 'year' THEN
    	SET date3 = date2 + INTERVAL duration YEAR;
    ELSE
    	SET date3 = date2 + INTERVAL duration SECOND;
    END IF;
    
    -- get res
    SET res = (
        (
            date3 >= date1
        )
    );
    
	RETURN res;
END$$

DROP FUNCTION IF EXISTS `timeIsOverUseDatenow`$$
CREATE DEFINER=`ntouba98`@`%` FUNCTION `timeIsOverUseDatenow` (`thedate` DATETIME, `duration` REAL, `timeType` ENUM('second','minute','hour','day','month','year')) RETURNS TINYINT(1) BEGIN
	RETURN timeIsOver(
        CURRENT_TIMESTAMP,
        thedate,
        duration,
        timetype
    );
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Structure de la table `test_datnek_lang`
--

DROP TABLE IF EXISTS `test_datnek_lang`;
CREATE TABLE IF NOT EXISTS `test_datnek_lang` (
  `code` varchar(30) NOT NULL,
  `lang` enum('en','fr','nl') NOT NULL,
  `spoken_level` enum('intermediate','pre_intermediate','current','elementary') NOT NULL,
  `written_level` enum('intermediate','pre_intermediate','current','elementary') NOT NULL,
  `understanding_level` enum('intermediate','pre_intermediate','current','elementary') NOT NULL,
  `date_added` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_updated` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` enum('visible','archived','blocked') NOT NULL DEFAULT 'visible',
  PRIMARY KEY (`code`),
  UNIQUE KEY `lang` (`lang`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Tronquer la table avant d'insérer `test_datnek_lang`
--

TRUNCATE TABLE `test_datnek_lang`;
--
-- Déchargement des données de la table `test_datnek_lang`
--

INSERT INTO `test_datnek_lang` (`code`, `lang`, `spoken_level`, `written_level`, `understanding_level`, `date_added`, `date_updated`, `status`) VALUES
('fr', 'fr', 'current', 'current', 'current', '2021-11-01 22:38:10', '2021-11-01 22:38:10', 'visible'),
('en', 'en', 'current', 'current', 'current', '2021-11-01 22:38:13', '2021-11-01 22:38:13', 'visible'),
('nl', 'nl', 'current', 'current', 'current', '2021-11-01 22:38:17', '2021-11-01 22:38:17', 'visible');

-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `view_test_datnek_lang`
-- (Voir ci-dessous la vue réelle)
--
DROP VIEW IF EXISTS `view_test_datnek_lang`;
CREATE TABLE IF NOT EXISTS `view_test_datnek_lang` (
`code` varchar(30)
,`lang` enum('en','fr','nl')
,`spoken_level` enum('intermediate','pre_intermediate','current','elementary')
,`written_level` enum('intermediate','pre_intermediate','current','elementary')
,`understanding_level` enum('intermediate','pre_intermediate','current','elementary')
,`date_added` datetime
,`date_updated` datetime
,`status` enum('visible','archived','blocked')
);

-- --------------------------------------------------------

--
-- Structure de la vue `view_test_datnek_lang`
--
DROP TABLE IF EXISTS `view_test_datnek_lang`;

CREATE ALGORITHM=UNDEFINED DEFINER=`ntouba98`@`%` SQL SECURITY DEFINER VIEW `view_test_datnek_lang`  AS  select `l`.`code` AS `code`,`l`.`lang` AS `lang`,`l`.`spoken_level` AS `spoken_level`,`l`.`written_level` AS `written_level`,`l`.`understanding_level` AS `understanding_level`,`l`.`date_added` AS `date_added`,`l`.`date_updated` AS `date_updated`,`l`.`status` AS `status` from `test_datnek_lang` `l` ;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
