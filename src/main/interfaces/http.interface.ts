export interface HttpConfigPossible {
    page: number;
    pagePossible: number[];
    precPagePossible: number | undefined;
    nextPagePossible: number | undefined;
    pages: string | number;
    pagesPossibles: (string | number)[];
    orderBy: string;
}

export interface HttpConfig {
    page: number;
    pages: string | number;
    orderBy?: string;
}