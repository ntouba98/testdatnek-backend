import { Router } from 'express';

export interface Main {
    url: string,
    router: Router
};
export interface Module {
    url: string,
    router: Router
};

export interface NotificationStruct {
    type: string;
    messages: {
        fr: string;
        en: string;
        nl: string;
    };
};
export interface NotificationFinalStruct {
    type: string;
    message: string;
};