import Joi from 'joi';
import { ReadImgBase64 } from './file';

export function GetSocialNetworkIcon(sn:string, choice:string) {
    const choices:string[] = ['icon', 'class'];
    choice = (
        choices.includes(choice)
    ) ? choice : 'icon';
    const icons:any = {
        facebook: {
            icon: 'fab fa-facebook-f',
            class: 'sn-facebook-hover',
        },
        twitter: {
            icon: 'fab fa-twitter',
            class: 'sn-twitter-hover',
        },
        linkedin: {
            icon: 'fab fa-linkedin-in',
            class: 'sn-linkedin-hover',
        },
        youtube: {
            icon: 'fab fa-youtube-in',
            class: 'sn-youtube-hover',
        },
        instagram: {
            icon: 'fab fa-instagram-in',
            class: 'sn-instagram-hover',
        },
        googleplus: {
            icon: 'fab fa-googleplus-in',
            class: 'sn-googleplus-hover',
        },
        snapchat: {
            icon: 'fab fa-snapchat-in',
            class: 'sn-snapchat-hover',
        },
        pinterest: {
            icon: 'fab fa-pinterest-in',
            class: 'sn-pinterest-hover',
        },
        whatsapp: {
            icon: 'fab fa-whatsapp-in',
            class: 'sn-whatsapp-hover',
        },
    };
    return (
        Object.keys(icons).includes(sn)
    ) ? icons[sn][choice] : 'warning_amber';
}

export function ShowImage(
    data:any,
    initial:string = 'icons/image_black_24dp.svg',
) {
    let res:any;
    if (
        (
            typeof data === 'object'
            && Array.isArray(data) === true
            && data.length > 0
        )
        || (
            typeof data === 'string'
            && data.length > 0
        )
    ) {
        res = ReadImgBase64(data);
    } else {
        res = initial;
    }
    return res;
}
export function ShowImageProfilePicture(data:any) {
    return ShowImage(data, 'icons/no-profile-picture.svg');
}
export function ShowCoverPost(data:any) {
    return ShowImage(data, 'images/img-post1.jpg');
}