
export const status = ['visible', 'blocked', 'archived'];
export const langs = ['en', 'fr', 'nl'];
export const levelsLang = [ "intermediate" , "pre_intermediate" , "current" , "elementary" ];

export const dateFormat1 = 'YYYY/MM/DD HH:mm:ss.SSSZ';
export const dateFormat2 = 'YYYY/MM/DD HH:mm:ss';
export const timeFormat1 = 'HH:mm:ss.SSSZ';
export const timeFormat2 = 'HH:mm:ss';
export const pagesPossibles = [25, 50, 100, 'all'];