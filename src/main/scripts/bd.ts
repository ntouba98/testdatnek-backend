import {
    Sequelize
    , Dialect
} from 'sequelize';
import SequelizeConf from '../../../sequelize.conf.json';
import * as InitModels from '../../main/models/init-models';

const sequelize = new Sequelize(SequelizeConf.database, SequelizeConf.username, SequelizeConf.password, {
    host: SequelizeConf.host,
    port: SequelizeConf.port,
    dialect: SequelizeConf.dialect as Dialect
});
export const models = InitModels.initModels(sequelize);