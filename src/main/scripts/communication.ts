import nodemailer from 'nodemailer';
import ConfigJSON from '../../config.json';

export class Email {
    senderName: string = ConfigJSON.email.username;
    transporterConfig:any = {
        host: ConfigJSON.email.host,
        port: ConfigJSON.email.port,
        secure: ConfigJSON.email.tls,
        auth: {
          user: ConfigJSON.email.user,
          pass: ConfigJSON.email.password,
        },
    };
    transporter: nodemailer.Transporter;

    constructor() {
        this.transporter = nodemailer.createTransport(this.transporterConfig);
    }

    ChangeSenderDatas(name?: string, mail?: string) {
        if(!name) {
            this.senderName = String(name);
        }
        if(!mail) {
            this.transporterConfig.auth.user = String(mail);
        }
        this.transporter = nodemailer.createTransport(this.transporterConfig);
        return this;
    }

    async SendEmail(
        recipient: string
        , subject: string
        , content: string
    ): Promise<boolean | unknown> {
        return new Promise((resolve,reject)=>{
            this.transporter.sendMail({
                from: `"${this.senderName}" <${this.transporterConfig.auth.user}>`,
                to: recipient,
                subject: subject,
                text: content,
            }, (err: Error | null, info: any) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(true);
                }
            });
        });
    }
    async SendHtmlEmail(
        recipient: string
        , subject: string
        , content: string
    ): Promise<boolean | unknown> {
        return new Promise((resolve,reject)=>{
            this.transporter.sendMail({
                from: `"${this.senderName}" ${this.transporterConfig.auth.user}`,
                to: recipient,
                subject: subject,
                html: content,
            }, (err: Error | null, info: any) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(true);
                }
            });
        });
    }

    async SendMultipleEmail(
        recipients: string[]
        , subject: string
        , content: string
    ): Promise<boolean | unknown> {
        return new Promise((resolve,reject)=>{
            this.transporter.sendMail({
                from: `"${this.senderName}" ${this.transporterConfig.auth.user}`,
                to: recipients.join(', '),
                subject: subject,
                text: content,
            }, (err: Error | null, info: any) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(true);
                }
            });
        });
    }
    async SendMultipleHtmlEmail(
        recipients: string[]
        , subject: string
        , content: string
    ): Promise<boolean | unknown> {
        return new Promise((resolve,reject)=>{
            this.transporter.sendMail({
                from: `"${this.senderName}" ${this.transporterConfig.auth.user}`,
                to: recipients.join(', '),
                subject: subject,
                html: content,
            }, (err: Error | null, info: any) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(true);
                }
            });
        });
    }
}