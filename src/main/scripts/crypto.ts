import CryptoJS from 'crypto-js';
import ConfigJSON from '../../config.json';
import { GetCurrentDate } from './date';
import { RandomF } from './string';

export class SHA{
    hashLength: 224 | 256 | 384 | 512 = 512;
    enc: 'base64' | 'hex' = 'base64';

    constructor(hashLength: 224 | 256 | 384 | 512 = 512, enc: 'base64' | 'hex' = 'base64') {
        this.hashLength = hashLength;
        this.enc = enc;
    }

    Hash(value: string, hashLength: 224 | 256 | 384 | 512 = 512, enc: 'base64' | 'hex' = 'base64') {
        this.hashLength = hashLength;
        this.enc = enc;
        let res2:any;
        if (this.hashLength === 512) {
            res2 = CryptoJS.HmacSHA512(value, ConfigJSON.crypto.key);
        } else if (this.hashLength === 384) {
            res2 = CryptoJS.HmacSHA384(value, ConfigJSON.crypto.key);
        } else if (this.hashLength === 256) {
            res2 = CryptoJS.HmacSHA256(value, ConfigJSON.crypto.key);
        } else if (this.hashLength === 224) {
            res2 = CryptoJS.HmacSHA224(value, ConfigJSON.crypto.key);
        } else {
            res2 = CryptoJS.HmacSHA512(value, ConfigJSON.crypto.key);
        }
        let res:string;
        if (this.enc === 'base64') {
            res = res2.toString(CryptoJS.enc.Base64);
        } else if (this.enc === 'hex') {
            res = res2.toString(CryptoJS.enc.Hex);
        } else {
            res = res2.toString(CryptoJS.enc.Base64);
        }
        return res;
    }
}

export class MD5{
    enc: 'base64' | 'hex' = 'base64';

    constructor(enc: 'base64' | 'hex' = 'base64') {
        this.enc = enc;
    }

    Hash(value: string, enc: 'base64' | 'hex' = 'base64') {
        this.enc = enc;
        let res2:any = CryptoJS.HmacMD5(value, ConfigJSON.crypto.key);
        let res:string;
        if (this.enc === 'base64') {
            res = res2.toString(CryptoJS.enc.Base64);
        } else if (this.enc === 'hex') {
            res = res2.toString(CryptoJS.enc.Hex);
        } else {
            res = res2.toString(CryptoJS.enc.Base64);
        }
        return res;
    }
}

export class AES{
    mode = CryptoJS.mode.CFB;
    padding = CryptoJS.pad.Pkcs7;
    key = CryptoJS.enc.Hex.parse(ConfigJSON.crypto.key);
    iv = CryptoJS.enc.Hex.parse(ConfigJSON.crypto.iv);
    enc: 'base64' | 'hex' | 'utf8' | 'initial' = 'utf8';

    constructor(enc: 'base64' | 'hex' | 'utf8' | 'initial' = 'utf8', mode = CryptoJS.mode.CFB, padding = CryptoJS.pad.Pkcs7) {
        this.mode = mode;
        this.padding = padding;
        this.enc = enc;
    }

    Encrypt(
        value: string | CryptoJS.lib.WordArray
        , enc: 'base64' | 'hex' | 'utf8' | 'initial' = 'utf8'
        , mode = CryptoJS.mode.CFB
        , padding = CryptoJS.pad.Pkcs7
    ) {
        this.mode = mode;
        this.padding = padding;
        this.enc = enc;
        let res2:CryptoJS.lib.CipherParams = CryptoJS.AES.encrypt(value, this.key, {
            iv: this.iv,
            mode: this.mode,
            padding: this.padding
        });
        let res:string | CryptoJS.lib.CipherParams = res2;
        if (['base64', 'utf8'].includes(this.enc)) {
            res = res2.toString();
        } else if (this.enc === 'hex') {
            res = res2.toString(CryptoJS.format.Hex);
        }
        return res;
    }
    Decrypt(
        value: string | CryptoJS.lib.CipherParams
        , enc: 'base64' | 'hex' | 'utf8' | 'initial' = 'utf8'
        , mode = CryptoJS.mode.CFB
        , padding = CryptoJS.pad.Pkcs7
    ) {
        this.mode = mode;
        this.padding = padding;
        this.enc = enc;
        const res2:CryptoJS.lib.WordArray = CryptoJS.AES.decrypt(value, this.key, {
            iv: this.iv,
            mode: this.mode,
            padding: this.padding
        });
        let res:string | CryptoJS.lib.WordArray = res2;
        if (this.enc === 'base64') {
            res = res2.toString(CryptoJS.enc.Base64);
        } else if (this.enc === 'utf8') {
            res = res2.toString(CryptoJS.enc.Utf8);
        } else if (this.enc === 'hex') {
            res = res2.toString(CryptoJS.enc.Hex);
        }
        return res;
    }
}

function Base64url(source: CryptoJS.lib.WordArray) {
    let encodedSource:string = CryptoJS.enc.Base64.stringify(source);
    encodedSource = encodedSource.replace(/=+$/, '');
    encodedSource = encodedSource.replace(/\+/g, '-');
    encodedSource = encodedSource.replace(/\//g, '_');

    return encodedSource;
}
export function GenerateToken(data:any) {
    if(typeof data === 'object' && Array.isArray(data) === false) {
        data['_token_date'] = GetCurrentDate();
    } else {
        data = {
            value: data,
            _token_date: GetCurrentDate(),
        };
    }
    const stringifiedHeader: CryptoJS.lib.WordArray = CryptoJS.enc.Utf8.parse(
        RandomF(5, 'alphabetical_s')
    );
    const encodedHeader = Base64url(stringifiedHeader);
      
    const stringifiedData = CryptoJS.HmacSHA512(JSON.stringify(data), ConfigJSON.crypto.key);
    const encodedData = Base64url(stringifiedData);
    
    return `${encodedHeader} ${encodedData}`;
}