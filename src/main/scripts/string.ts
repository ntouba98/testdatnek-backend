

export function RandomF(
    size:number = 10
    , type: 'alphanumeric' | 'alphanumeric_s' | 'alphabetical' | 'alphabetical_s' | 'numeric' = 'alphanumeric'
    , formatting: (data: string) => string = (data: string) => data
) {
    const types:string[] = ['alphanumeric', 'alphanumeric_s', 'alphabetical', 'alphabetical_s', 'numeric'];
    const valuesTypesNumeric:number[] = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9];
    const valuesTypesAlphabetical:string[] = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'];
    const valuesTypesAlphabeticalS:string[] = valuesTypesAlphabetical.map((value) => value.toUpperCase()).concat(
        valuesTypesAlphabetical,
    );
    const valuesTypesAlphanumeric:string[] = valuesTypesNumeric.map((value:number) => String(value)).concat(valuesTypesAlphabetical);
    const valuesTypesAlphanumericS:string[] = valuesTypesNumeric.map((value:number) => String(value)).concat(valuesTypesAlphabeticalS);

    size = (
        typeof size === 'number'
        && size > 1
    ) ? size : 5;
    const valuesTypes: any = {
        numeric: valuesTypesNumeric,
        alphabetical: valuesTypesAlphabetical,
        alphanumeric: valuesTypesAlphanumeric,
        alphabetical_s: valuesTypesAlphabeticalS,
        alphanumeric_s: valuesTypesAlphanumericS,
    };

    const min = 1;
    const max = valuesTypes[type].length;
    let result:string[] = [];
    for (let index = 0; index < size; index++) {
        result.push(
            valuesTypes[type][
                Math.floor(Math.random() * max) + min
            ],
        );
    }

    return formatting(result.join(''));
}

export function UcFirst(value: any):string {
    if (!value) return '';
    value = String(value);
    return value.charAt(0).toUpperCase() + value.slice(1).toLowerCase();
}
export function Upper(value: any):string {
    if (!value) return '';
    value = String(value);
    return value.toUpperCase();
}
export function Lower(value: any):string {
    if (!value) return '';
    value = String(value);
    return value.toLowerCase();
}
export function Capitalize(value: any):string {
    const sep = ' ';
    if (!value) return '';
    const array_res:string[] = String(value).split(sep).map((val:string) => val.charAt(0).toUpperCase() + val.toLowerCase().slice(1));
    const res:string = array_res.join(sep);
    return res;
}

export function ArrayToRegExp(values: any[]) {
    return new RegExp(values.join('|'))
}

export function SearchForFind(value: string | undefined): string[] {
    return (
        value
        && typeof(value) === 'string'
        && value.length > 0
    ) ? Lower(value).replace(/[^\w\s]/gi, '').replace(/\s/gi, '+').split('+').filter((value: string) => (
        typeof(value) === 'string'
        && value.length > 0
    )) : [];
}