
export function RemovePointer(element: any) {
    let result:any;
    if (
        typeof element === 'object'
    ) {
        result = (Array.isArray(element) === true) ? [
            ...element,
        ] : {
            ...element,
        };
    } else {
        result = element;
    }

    if (
        typeof result === 'object'
    ) {
        const res = result;
        Object.keys(res).forEach((key) => {
            result[key] = RemovePointer(res[key]);
        });
    }

    return result;
}

export function MapAll(
    element: any
    , mapper: (data: any) => any = (data: any) => data
) {
    let result: any;
    
    if (
        typeof element === 'object'
    ) {
        result = element;
        Object.keys(result).forEach((key) => {
            result[key] = MapAll(result[key], mapper);
        });
    } else {
        result = mapper(element);
    }

    return result;
}

export function GetBodyForUpdate(initialBody: any, newBody: any, nullableProperties: string[] = []): any {
    let result: any;
    
    console.log('----> GetBodyForUpdate > initialBody:: ', initialBody);
    console.log('----> GetBodyForUpdate > newBody:: ', newBody);
    if (
        typeof initialBody === 'object'
        && Array.isArray(initialBody) === false
        && typeof newBody === 'object'
        && Array.isArray(newBody) === false
    ) {
        result = initialBody;
        Object.keys(newBody).forEach((key, index) => {
            if (
                initialBody[key] instanceof Date
                && !(newBody[key] instanceof Date)
            ) {
                newBody[key] = undefined;
            }
            if (
                (
                    Object.keys(result).includes(key)
                    && result[key] != newBody[key]
                    && typeof(result[key]) == typeof(newBody[key])
                    && newBody[key]
                ) || (
                    nullableProperties.includes(key)
                    && newBody[key] === null
                )
            ) {
                result[key] = newBody[key];
            }
        });
    } else {
        result = initialBody;
    }

    return result;
}
export function GetBodyForUpdate2(initialBody: any, newBody: any): any {
    let result: any;
    
    console.log('----> GetBodyForUpdate2 > initialBody:: ', initialBody);
    console.log('----> GetBodyForUpdate2 > newBody:: ', newBody);
    if (
        typeof initialBody === 'object'
        && Array.isArray(initialBody) === false
        && typeof newBody === 'object'
        && Array.isArray(newBody) === false
    ) {
        result = initialBody;
        console.log('----> GetBodyForUpdate2 > Object.keys(newBody):: ', Object.keys(newBody));
        console.log('----> GetBodyForUpdate2 > Object.values(newBody):: ', Object.values(newBody));
        Object.keys(newBody).forEach((key, index) => {
            console.log('----> GetBodyForUpdate2 > index::', index, ' - key:: ', key);
            if (
                initialBody[key] instanceof Date
                && !(newBody[key] instanceof Date)
            ) {
                newBody[key] = undefined;
            }
            if (
                (
                    newBody[key]
                )
            ) {
                result[key] = newBody[key];
            }
        });
    } else {
        result = initialBody;
    }

    return result;
}