
export function GetCustomMonetaryAmount(value:any, lang:string = 'en', simplified:boolean = true) {
    lang = (['en', 'fr'].includes(lang)) ? lang : 'en';
    simplified = (typeof simplified === 'boolean') ? simplified : false;
    value = (
        (
            typeof value === 'string'
            && value.length > 0
        ) || typeof value === 'number'
    ) ? parseFloat(String(value)) : 0;

    const paramsConfig:any[] = [
        {
            min: 0,
            max: (10 ** 3) - 1,
            labelSimplified: undefined,
            label: undefined,
        },
        {
            min: (10 ** 3),
            max: (10 ** 6) - 1,
            labelSimplified: 'k',
            label: {
                fr: 'mille',
                en: 'thousand',
            },
            labelPluralSup: {
                fr: 's',
                en: undefined,
            },
        },
        {
            min: (10 ** 6),
            max: (10 ** 9) - 1,
            labelSimplified: 'm',
            label: {
                fr: 'million',
                en: 'million',
            },
            labelPluralSup: {
                fr: 's',
                en: undefined,
            },
        },
        {
            min: (10 ** 9),
            max: (10 ** 12) - 1,
            labelSimplified: 'md',
            label: {
                fr: 'milliard',
                en: 'billion',
            },
            labelPluralSup: {
                fr: 's',
                en: undefined,
            },
        },
        {
            min: (10 ** 12),
            labelSimplified: 'b',
            label: {
                fr: 'billion',
                en: 'trillion',
            },
            labelPluralSup: {
                fr: 's',
                en: undefined,
            },
        },
    ];

    const paramConfig:any = paramsConfig.find((elm:any) => (
        (
            !elm.min
            || value >= elm.min
        ) && (
            !elm.max
            || value <= elm.max
        )
    ));
    value = (paramConfig.min === 0) ? value : (value / paramConfig.min);
    value = Math.round(value * 100) / 100;
    const valueLabel:string = (simplified) ? paramConfig.labelSimplified : [
        paramConfig.label[lang],
        (value > 1) ? paramConfig.labelPluralSup[lang] : undefined,
    ].filter((val) => typeof val === 'string' && val.length > 0).join('');

    // // console.log('---- lang:: ', lang);
    // // console.log('---- simplified:: ', simplified);
    // // console.log('---- value:: ', value);
    // // console.log('---- paramsConfig:: ', paramsConfig);
    // // console.log('---- paramConfig:: ', paramConfig);
    // // console.log('---- valueLabel:: ', valueLabel);

    const valueFinal:string = [
        value,
        valueLabel,
    ].filter((val) => (
        (typeof val === 'string' && val.length > 0)
        || typeof val === 'number'
    )).join((simplified) ? ' ' : ' ');
    return valueFinal;
}