import moment from 'moment';
import { dateFormat1, dateFormat2 } from './config';

const padLeft: (data: any, base?: number, chr?: string) => any = (data, base = 10, chr = '0') => {
    const len = (String(base || 10).length - String(data).length) + 1;
    return len > 0 ? new Array(len).join(chr || '0') + data : data;
};

export function GetCustomTime(initialDateStr:string, lang:'fr' | 'en' = 'en', dateFormated:boolean = false) {
    let initialDate:any;
    if (dateFormated) {
        initialDate = moment(initialDateStr).toDate();
    } else {
        try {
            initialDate = new Date(initialDateStr);
        } catch (error) {
            initialDate = undefined;
        }
    }
    const currentDate:any = new Date();
    const difference = (
        initialDate && currentDate
    ) ? Math.abs(currentDate - initialDate) : 0;
    const differenceList: any = {
        second: {
            value: difference / 10 ** 3,
            limit: 60,
            sup: false,
            sup_str: {
                fr: 'séconde',
                en: 'second',
            },
            sup_str_multi: {
                fr: 's',
                en: 's',
            },
            exists: undefined,
        },
        minute: {
            value: difference / (6 * 10 ** 4),
            limit: 60,
            sup: false,
            sup_str: {
                fr: 'minute',
                en: 'minute',
            },
            sup_str_multi: {
                fr: 's',
                en: 's',
            },
            exists: undefined,
        },
        hour: {
            value: difference / (36 * 10 ** 5),
            limit: 24,
            sup: false,
            sup_str: {
                fr: 'heure',
                en: 'your',
            },
            sup_str_multi: {
                fr: 's',
                en: 's',
            },
            exists: undefined,
        },
        day: {
            value: difference / (864 * 10 ** 5),
            limit: 7,
            sup: false,
            sup_str: {
                fr: 'jour',
                en: 'day',
            },
            sup_str_multi: {
                fr: 's',
                en: 's',
            },
            exists: undefined,
        },
        week: {
            value: difference / ((864 * 10 ** 5) * 7),
            limit: 4.5,
            sup: false,
            sup_str: {
                fr: 'semaine',
                en: 'week',
            },
            sup_str_multi: {
                fr: 's',
                en: 's',
            },
            exists: undefined,
        },
        month: {
            value: difference / (26298 * 10 ** 5),
            limit: 12,
            sup: false,
            sup_str: {
                fr: 'mois',
                en: 'month',
            },
            sup_str_multi: {
                fr: '',
                en: 's',
            },
            exists: undefined,
        },
        year: {
            value: difference / (315576 * 10 ** 5),
            limit: 100,
            sup: false,
            sup_str: {
                fr: 'année',
                en: 'year',
            },
            sup_str_multi: {
                fr: 's',
                en: 's',
            },
            exists: undefined,
        },
        century: {
            value: difference / ((315576 * 10 ** 5) * 100),
            limit: 100,
            sup: true,
            sup_str: {
                fr: 'siècle',
                en: 'century',
            },
            sup_str_multi: {
                fr: 's',
                en: 's',
            },
            exists: undefined,
        },
    };
    const inf:any = {
        fr: 'il y a',
        en: 'there is',
    };
    // // console.log('---- GetCustomTime > old - differenceList:: ', differenceList);
    Object.keys(differenceList).forEach((key, index) => {
        const initialValue = differenceList[key];
        if (initialValue.sup) {
            initialValue.exists = initialValue.value >= initialValue.limit;
        } else {
            initialValue.exists = initialValue.value < initialValue.limit && initialValue.value >= 1;
        }
        const val = Math.floor(initialValue.value);
        let sup = initialValue.sup_str[lang];
        sup = (val > 1) ? sup + initialValue.sup_str_multi[lang] : sup;
        initialValue.final_value = `${inf[lang]} ${val} ${sup}`;
        differenceList[key] = initialValue;
    });

    // // console.log('---- GetCustomTime > ICI ----');
    // // console.log('---- GetCustomTime > initialDateStr:: ', initialDateStr);
    // // console.log('---- GetCustomTime > initialDate:: ', initialDate);
    // // console.log('---- GetCustomTime > typeof initialDate:: ', typeof initialDate);
    // // console.log('---- GetCustomTime > currentDate:: ', currentDate);
    // // console.log('---- GetCustomTime > typeof currentDate:: ', typeof currentDate);
    // // console.log('---- GetCustomTime > difference:: ', difference);
    // // console.log('---- GetCustomTime > typeof difference:: ', typeof difference);
    // // console.log('---- GetCustomTime > differenceList:: ', differenceList);

    const result:any = (
        typeof(differenceList) === 'object'
        && Array.isArray(differenceList) === false
    ) ? Object.values(differenceList).find((x:any) => x.exists) : {};
    // // console.log('---- GetCustomTime > result:: ', result);
    return (result) ? result.final_value : undefined;
}
export function GetCustomTime2(initialDateStr:string, lang:'fr' | 'en' = 'en', showTime:boolean = false, simplified:boolean = true, simplifiedLength:number = 3) {
    showTime = (typeof showTime === 'boolean') ? showTime : false;
    simplified = (typeof simplified === 'boolean') ? simplified : false;
    simplifiedLength = (
        typeof simplifiedLength === 'number'
        && simplifiedLength > 0
    ) ? simplifiedLength : 3;
    lang = (
        typeof lang === 'string'
        && ['en', 'fr'].includes(lang)
    ) ? lang : 'en';
    let initialDate:any;
    try {
        initialDate = new Date(initialDateStr);
    } catch (error) {
        initialDate = undefined;
    }

    const weekDays:any = [
        {
            fr: 'lundi',
            en: 'monday',
        },
        {
            fr: 'mardi',
            en: 'tuesday',
        },
        {
            fr: 'mercredi',
            en: 'wednesday',
        },
        {
            fr: 'jeudi',
            en: 'thursday',
        },
        {
            fr: 'vendredi',
            en: 'friday',
        },
        {
            fr: 'samedi',
            en: 'saturday',
        },
        {
            fr: 'dimanche',
            en: 'sunday',
        },
    ];
    const months:any = [
        {
            fr: 'janvier',
            en: 'january',
        },
        {
            fr: 'fevrier',
            en: 'febuary',
        },
        {
            fr: 'mars',
            en: 'march',
        },
        {
            fr: 'avril',
            en: 'april',
        },
        {
            fr: 'mai',
            en: 'may',
        },
        {
            fr: 'juin',
            en: 'june',
        },
        {
            fr: 'juillet',
            en: 'july',
        },
        {
            fr: 'août',
            en: 'august',
        },
        {
            fr: 'septembre',
            en: 'september',
        },
        {
            fr: 'octobre',
            en: 'october',
        },
        {
            fr: 'novembre',
            en: 'november',
        },
        {
            fr: 'decembre',
            en: 'december',
        },
    ];

    let initialDateWeekDay:number = initialDate.getUTCDay() - 1;
    initialDateWeekDay = (initialDateWeekDay > 0) ? initialDateWeekDay : 6;
    initialDateWeekDay = (!simplified) ? weekDays[initialDateWeekDay][lang] : `${weekDays[initialDateWeekDay][lang].substring(0, simplifiedLength)}.`;
    let initialDateMonth:number = initialDate.getUTCMonth();
    initialDateMonth = (!simplified) ? months[initialDateMonth][lang] : `${months[initialDateMonth][lang].substring(0, simplifiedLength)}.`;
    const initialDateTime = (showTime) ? [
        padLeft(initialDate.getUTCHours().toString()),
        padLeft(initialDate.getUTCMinutes().toString()),
        padLeft(initialDate.getUTCSeconds().toString()),
    ].join(':') : '';
    const dateFinal = [
        initialDateWeekDay,
        [
            [
                padLeft(initialDate.getUTCDate()),
                initialDateMonth,
                padLeft(initialDate.getFullYear()),
            ].join(' '),
            initialDateTime,
        ].join(' '),
    ].join(' ');

    // // console.log('---- ICI ----');
    // // console.log(`---- initialDate:: ${initialDate}`);
    // // console.log(`---- initialDateWeekDay:: ${initialDateWeekDay}`);
    // // console.log(`---- initialDateMonth:: ${initialDateMonth}`);
    // // console.log(`---- initialDateTime:: ${initialDateTime}`);
    // // console.log(`---- dateFinal:: ${dateFinal}`);

    return dateFinal;
}

export function GetCurrentDate(string: boolean = false):Date | string {
    const format:string = dateFormat1;
    const today = new Date();
    const res = `${padLeft(today.getUTCFullYear().toString())}/${padLeft(today.getUTCMonth().toString()+1)}/${padLeft(today.getUTCDate().toString())} ${padLeft(today.getUTCHours().toString())}:${padLeft(today.getUTCMinutes().toString())}:${padLeft(today.getUTCSeconds().toString())}.${padLeft(today.getUTCMilliseconds().toString())}Z`;
    return (string) ? moment(
        moment(res, format).toDate()
    ).format(format) : moment(res, format).toDate();
}

export function DateToString(value:Date, format:string = dateFormat2): string {
    return moment(value).format(format);
}
export function StringToDate(value:string, format:string = dateFormat2): Date {
    return moment(value, format).toDate();
}
export function StringToDate2(value:string): Date {
    return moment(value).toDate();
}