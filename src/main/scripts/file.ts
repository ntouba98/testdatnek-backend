
export function ReadImgBase64(data:any) {
    let result:any;
    if (
        typeof (data) === 'object'
        && Array.isArray(data) === true
    ) {
        result = data.join('');
    } else if (
        typeof (data) === 'string'
        && data.length > 0
    ) {
        result = data;
    }
    return `${result}`;
}

export function ConvertBase64(data: any) {
    const lenMax:number = 100000;
    let result:any = [];
    if (
        typeof (data) === 'string'
        && data.length > 0
    ) {
        result = (data.length > 0) ? (
            ((data.length % lenMax) > 0) ? Math.floor(data.length / lenMax) + 1 : Math.floor(data.length / lenMax)
        ) : 0;
        result = Array.from(
            Array(
                result,
            ),
        ).map((value, index) => index * lenMax)
        .map((value) => ({ e1: value, e2: value + lenMax }))
        .map((value) => data.substring(value.e1, value.e2));
    } else if (
        typeof (data) === 'object'
        && Array.isArray(data) === true
    ) {
        result = data;
    }
    return result;
}
export function ConvertBase64Object(data: any) {
    data = (
        typeof data === 'object'
        && Array.isArray(data) === false
    ) ? data : {
        value: [],
    };

    let value: any = [];
    if (
        typeof data.value === 'object'
        && Array.isArray(data.value) === true
    ) {
        value = data.value;
    } else if (
        typeof data.value === 'string'
        && data.value.length > 0
    ) {
        value = data.value.split('/');
    }
    data.value = value;
    return data;
}