import Joi from 'joi';
import { RemovePointer } from './json';

export function ValidForm(schema:any, element:object) {
    const elementFinal = RemovePointer(element);
    const result = Joi.object(schema).validate(elementFinal);
    // console.log('******----- > ValidForm - schema:: ', schema);
    // console.log('******----- > ValidForm - element:: ', element);
    // console.log('******----- > ValidForm - schemaFinal:: ', schemaFinal);
    // console.log('******----- > ValidForm - elementFinal:: ', elementFinal);
    console.log('******----- > ValidElementForm - result:: ', result);
    return !(typeof (result) === 'object' && Object.keys(result).includes('error'));
}
export function GetValidForm(schema:any, element:object) {
    const elementFinal = RemovePointer(element);
    const result = Joi.object(schema).validate(elementFinal);
    return (!(typeof (result) === 'object' && Object.keys(result).includes('error'))) ? result.value : undefined;
}

export function ValidElementForm(schema:any, element:any) {
    const schemaFinal = {
        value: schema,
    };
    const elementFinal = {
        value: RemovePointer(element),
    };
    return ValidForm(schemaFinal, elementFinal);
}