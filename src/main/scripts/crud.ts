import { Op } from 'sequelize';
import SequelizeConf from '../../../sequelize.conf.json';
import { NotificationFinalStruct, NotificationStruct } from "../../main/interfaces/struct.interface";
import { langs, pagesPossibles } from './config';
import { GetValidForm, ValidForm } from "./forms";
import { GetBodyForUpdate, GetBodyForUpdate2 } from './json';
import { Lower } from './string';


export class CRUD {
    _lang: 'en' | 'fr' | 'nl' = 'fr';
    nullablePropertiesForUpdate: string[] = [];
    nullablePropertiesForEdit: string[] = [];
    findAll_orderBy: any = {
        code_desc: {
            value: 'code'
            , meaning: 'desc'
        }
        , code_asc: {
            value: 'code'
            , meaning: 'asc'
        }
        , date_added_asc: {
            value: 'date_added'
            , meaning: 'asc'
        }
        , date_added_desc: {
            value: 'date_added'
            , meaning: 'desc'
        }
    };

    notifications:any = {
        good_restored: {
            type: 'success',
            messages: {
                fr: 'l\'element a été restauré avec succès',
                en: 'item has been successfully restored',
                nl: 'item has been successfully restored',
            },
        },
        good_blocked: {
            type: 'success',
            messages: {
                fr: 'l\'element a été bloqué avec succès',
                en: 'the item was blocked successfully',
                nl: 'item has been successfully restored',
            },
        },
        good_archived: {
            type: 'success',
            messages: {
                fr: 'l\'element a été archivé avec succès',
                en: 'the item was archived successfully',
                nl: 'item has been successfully restored',
            },
        },
        good_add: {
            type: 'success',
            messages: {
                fr: 'création de l\'element réussi avec succès',
                en: 'successful element creation',
                nl: 'item has been successfully restored',
            },
        },
        good_update: {
            type: 'success',
            messages: {
                fr: 'Modification de l\'element réussi avec succès',
                en: 'Element modification successfully completed',
                nl: 'item has been successfully restored',
            },
        },
        good_edit: {
            type: 'success',
            messages: {
                fr: 'Edition de l\'element réussi avec succès',
                en: 'Edit element successfully completed',
                nl: 'item has been successfully restored',
            },
        },
        invalid_data: {
            type: 'warning',
            messages: {
                fr: 'la donnée est invalide',
                en: 'the data is invalid',
                nl: 'item has been successfully restored',
            },
        },
        not_found_data: {
            type: 'danger',
            messages: {
                fr: 'la donnée n\'existe pas',
                en: 'the data does not exist',
                nl: 'item has been successfully restored',
            },
        },
        found_data: {
            type: 'danger',
            messages: {
                fr: 'la donnée existe déjà',
                en: 'the data exists',
                nl: 'item has been successfully restored',
            },
        },
        unknown_error: {
            type: 'danger',
            messages: {
                fr: 'erreur inconnue , interne et signalé qui sera resolue dans les plus brefs delais',
                en: 'unknown, internal and reported error which will be resolved as soon as possible',
                nl: 'item has been successfully restored',
            },
        },
    };

    public constructor(lang: string = 'fr') {
        try {
            if (langs.includes(lang)) {
                this._lang = lang as ('en' | 'fr' | 'nl');
            } else {
                this._lang = 'fr';
            }
            console.log('----> constructor > this._lang:: ', this._lang);
        } catch (error) {
            console.log('----> constructor > error:: ', error);
        }
    }
    
    public async _findAll(
        model: any
        , params: any = {}
        , paramsConfig: any = {}
        , operatorParams: symbol = Op.or
        , page: number = 1
        , pages: string | number = 25
        , orderBy: string = 'date_added_asc'
        , progressive: boolean = false
    ): Promise<{
        datas: any[];
        lenMax: number;
        len: number;
        exists: boolean;
        page: number;
        pagePossible: number[];
        precPagePossible: number | undefined;
        nextPagePossible: number | undefined;
        pages: string | number;
        pagesPossibles: (string | number)[];
        orderBy: string;
    }> {
        let res: any;
        params = (
            typeof(params) === 'object'
            && Array.isArray(params) === false
        ) ? params : {};
        paramsConfig = (
            typeof(paramsConfig) === 'object'
            && Array.isArray(paramsConfig) === false
        ) ? paramsConfig : {};
        page = (typeof page === 'number' && page > 0) ? page : 1;
        pages = (pagesPossibles.includes(pages)) ? pages : 25;
        pages = (progressive && !(pages === 'all')) ? parseInt(String(pages)) * page : pages;
        page = (progressive) ? 1 : page;
        let pagePossible: number[] = [];
        let precPagePossible: number | undefined = undefined;
        let nextPagePossible: number | undefined = undefined;
        let offset: number = (!(pages === 'all') && page > 1) ? (page * parseInt(String(pages))) - parseInt(String(pages)) : 0;
        let limit: number | undefined = (pages === 'all') ? undefined : parseInt(String(pages));
        let orderByLabel = Object.keys(this.findAll_orderBy).find((value: string) => value === orderBy);
        orderByLabel = (orderByLabel) ? orderByLabel : 'date_added_asc';
        const orderByValue: {
            value: string;
            meaning: 'asc' | 'desc'
        } = this.findAll_orderBy[orderByLabel];
        let len: number = 0;
        let lenDatas: number = 0;
        try {
            let condFinal: any[] = [];
            Object.keys(paramsConfig).forEach((key)=> {
                if(
                    Object.keys(params).includes(key)
                    && typeof(paramsConfig[key]) === 'object'
                    && Array.isArray(paramsConfig[key]) === false
                    && Object.keys(paramsConfig[key]).includes('funct')
                    && Object.keys(paramsConfig[key]).includes('simplified')
                ) {
                    if(!(typeof(paramsConfig[key].simplified) === 'boolean')) {
                        paramsConfig[key].simplified = true;
                    }
                    const funct = paramsConfig[key].funct;
                    const simplified = paramsConfig[key].simplified;
                    const resultFunct = funct(params[key], simplified);
                    if (
                        resultFunct
                    ) {
                        condFinal = condFinal.concat(
                            [resultFunct]
                        );
                    }
                }
            });

            let finalParams2: any = {
            };
            let finalParams: any = {
                order: [
                    [orderByValue.value, orderByValue.meaning]
                ],
            };
            if (limit) {
                finalParams.limit = limit;
                finalParams.offset = offset;
            }
            if (condFinal.length > 0) {
                finalParams2.where = {
                    [operatorParams]: condFinal
                };
                finalParams.where = {
                    [operatorParams]: condFinal
                };
            }

            len = await model.count(finalParams2);
            res = await model.findAll(finalParams);
            lenDatas = res.length;

            page = (!progressive && offset > len) ? 1 : page;
            pages = (progressive && limit && limit > len) ? 25 : pages;

            const pagePossibleStep0: number = len / parseFloat(String((pages === 'all') ? len : pages));
            const pagePossibleStep1: number = Math.trunc(pagePossibleStep0);
            const pagePossibleStep2: number = (pagePossibleStep0) - pagePossibleStep1; 
            const pagePossibleStep3: number = (pagePossibleStep2 > 0) ? pagePossibleStep1 + 1 : pagePossibleStep1;
            pagePossible = Array.from(Array(pagePossibleStep3).keys()).map((value: number) => value + 1);
            precPagePossible = pagePossible.find((value: number, index: number) => index === 0);
            nextPagePossible = pagePossible.reverse().find((value: number, index: number) => index === 0);
            pagePossible = pagePossible.filter((value: number) => (
                value >= page - 2
                || value <= page + 2
            ));
            
            console.log('----> _findAll > pagePossible:: ', pagePossible);
            console.log('----> _findAll > condFinal:: ',condFinal);
            console.log('----> _findAll > page:: ', page);
            console.log('----> _findAll > pages:: ', pages);
            console.log('----> _findAll > orderByValue:: ', orderByValue);
        } catch (error) {
            console.log('----> _findAll > error:: ', error);
            res = [];
        }

        return {
            datas: res,
            len: lenDatas,
            lenMax: len,
            exists: res.length > 0,
            page: page,
            pagePossible: pagePossible,
            precPagePossible: precPagePossible,
            nextPagePossible: nextPagePossible,
            pages: pages,
            pagesPossibles: pagesPossibles,
            orderBy: orderByLabel,
        };
    }

    public async _exists<InterfaceBody>(
        value: any
        , getIdsFunct: (value: InterfaceBody) => any
        , findCondFunct: (ids: any)=> any
        , model: any
    ): Promise<{
        exists: boolean;
    }> {
        return {
            exists: (await this._find<InterfaceBody>(value, getIdsFunct, findCondFunct, model)).exists
        };
    }
    public async _find<InterfaceBody>(
        value: any
        , getIdsFunct: (value: InterfaceBody) => any
        , findCondFunct: (ids: any)=> any
        , model: any
    ): Promise<{
        data?: any;
        exists: boolean;
    }> {
        let res: any;
        try {
            const ids = getIdsFunct(value);
            const findCond = findCondFunct(ids);
            const elementExists: number = await model.count({
                where: findCond,
            });

            console.log('----> _find > value:: ', value);
            console.log('----> _find > ids:: ', ids);
            console.log('----> _find > findCond:: ', findCond);
            console.log('----> _find > elementExists:: ', elementExists);

            if (elementExists > 0) {
                const res1 = await model.findOne({
                    where: findCond,
                });
                res = (res1) ? res1.dataValues : undefined;

                console.log('----> _find > res:: ', res);
            }
        } catch (error) {
            console.log('----> _find > error:: ', error);
            res = undefined;
        }

        return {
            data: res,
            exists: res ? true : false
        };
    }

    public async _changeStatus<InterfaceBody>(
        value: any
        , statusValue: 'visible' | 'archived' | 'blocked' = 'visible'
        , getIdsFunct: (value: InterfaceBody) => any
        , findCondFunct: (ids: any)=> any
        , model: any
    ): Promise<NotificationFinalStruct> {
        let res: NotificationStruct;
        try {
            const ids = getIdsFunct(value);
            const findCond = findCondFunct(ids);
            const elementExists: number = await model.count({
                where: findCond,
            });

            console.log('----> _changeStatus > value:: ', value);
            console.log('----> _changeStatus > ids:: ', ids);
            console.log('----> _changeStatus > findCond:: ', findCond);
            console.log('----> _changeStatus > elementExists:: ', elementExists);
            console.log('----> _changeStatus > statusValue:: ', statusValue);

            if (elementExists > 0) {
                await model.update({
                    status: statusValue
                }, {
                    where: findCond,
                });
                
                if(statusValue == 'visible') {
                    res = this.notifications.good_restored;
                } else if(statusValue == 'archived') {
                    res = this.notifications.good_archived;
                } else if(statusValue == 'blocked') {
                    res = this.notifications.good_blocked;
                } else {
                    res = this.notifications.good_archived;
                }

                console.log('----> _changeStatus > res:: ', res);
            } else {
                res = this.notifications.not_found_data;
            }
        } catch (error) {
            console.log('----> _changeStatus > error:: ', error);
            res = this.notifications.unknown_error;
        }

        return {
            type: res.type,
            message: res.messages[this._lang],
        };
    }
    
    public async _edit<InterfaceBody, InterfaceModel, TypeModel, TypeFormSchema = any>(
        value: any
        , initBodyFunct: (value: unknown)=> InterfaceBody
        , getIdsFunct: (value: InterfaceBody) => any
        , findCondFunct: (ids: any)=> any
        , model: any
        , cleanBodyFunct: (value: any)=> InterfaceModel
        , formSchema: TypeFormSchema
    ): Promise<NotificationFinalStruct> {
        let res: NotificationStruct;
        try {
            value = initBodyFunct(value);
            console.log('----> _edit > value:: ', value);
            if(ValidForm(formSchema, value)) {
                res = await this._exec_edit<InterfaceBody, InterfaceModel>(GetValidForm(formSchema, value), getIdsFunct, findCondFunct, model, cleanBodyFunct);
            } else {
                res = this.notifications.invalid_data;
            }
        } catch (error) {
            console.log('----> _edit > error:: ', error);
            res = this.notifications.unknown_error;
        }

        return {
            type: res.type,
            message: res.messages[this._lang],
        };
    }
    public async _exec_edit<InterfaceBody, InterfaceModel>(
        value: any
        , getIdsFunct: (value: InterfaceBody) => any
        , findCondFunct: (ids: any)=> any
        , model: any
        , cleanBodyFunct: (value: any)=> InterfaceModel
    ): Promise<NotificationStruct> {
        let res: NotificationStruct;
        const ids = getIdsFunct(value);
        const findCond = findCondFunct(ids);
        const elementExists: number = await model.count({
            where: findCond,
        });
        if (elementExists > 0) {
            delete value.date_added;
            const cleanBody: InterfaceModel = cleanBodyFunct(
                GetBodyForUpdate(
                    (await model.findOne({
                        where: findCond,
                    })).dataValues
                    , value
                    , this.nullablePropertiesForEdit
                )
            );
            console.log('----> _exec_edit > cleanBody:: ', cleanBody);
            await model.update(cleanBody, {
                where: findCond,
            });
        } else {
            const cleanBody: InterfaceModel = cleanBodyFunct(value);
            console.log('----> _exec_edit > cleanBody:: ', cleanBody);
            await model.create(cleanBody);
        }
        res = this.notifications.good_edit;
        console.log('----> _exec_edit > ids:: ', ids);
        console.log('----> _exec_edit > findCond:: ', findCond);
        console.log('----> _exec_edit > elementExists:: ', elementExists);

        return res;
    }

    public async _add<InterfaceBody, InterfaceModel, TypeModel, TypeFormSchema = any>(
        value: any
        , initBodyFunct: (value: unknown)=> InterfaceBody
        , getIdsFunct: (value: InterfaceBody) => any
        , findCondFunct: (ids: any)=> any
        , model: any
        , cleanBodyFunct: (value: any)=> InterfaceModel
        , formSchema: TypeFormSchema
    ): Promise<NotificationFinalStruct> {
        let res: NotificationStruct;
        try {
            value = initBodyFunct(value);
            console.log('----> _add > value:: ', value);
            if(ValidForm(formSchema, value)) {
                res = await this._exec_add<InterfaceBody, InterfaceModel>(GetValidForm(formSchema, value), getIdsFunct, findCondFunct, model, cleanBodyFunct);
            } else {
                res = this.notifications.invalid_data;
            }
        } catch (error) {
            console.log('----> _add > error:: ', error);
            res = this.notifications.unknown_error;
        }

        return {
            type: res.type,
            message: res.messages[this._lang],
        };
    }
    public async _exec_add<InterfaceBody, InterfaceModel>(
        value: any
        , getIdsFunct: (value: InterfaceBody) => any
        , findCondFunct: (ids: any)=> any
        , model: any
        , cleanBodyFunct: (value: any)=> InterfaceModel
    ): Promise<NotificationStruct> {
        let res: NotificationStruct;
        const ids = getIdsFunct(value);
        const findCond = findCondFunct(ids);
        const elementExists: number = await model.count({
            where: findCond,
        });
        if (elementExists > 0) {
            res = this.notifications.found_data;
        } else {
            const cleanBody: InterfaceModel = cleanBodyFunct(value);
            await model.create(cleanBody);
            console.log('----> _exec_add > ids:: ', ids);
            console.log('----> _exec_add > cleanBody:: ', cleanBody);
            console.log('----> _exec_add > findCond:: ', findCond);
            console.log('----> _exec_add > elementExists:: ', elementExists);
            res = this.notifications.good_add;
        }

        return res;
    }

    public async _update<InterfaceBody, InterfaceModel, TypeModel, TypeFormSchema = any>(
        value: any
        , initBodyFunct: (value: unknown)=> InterfaceBody
        , getIdsFunct: (value: InterfaceBody) => any
        , findCondFunct: (ids: any)=> any
        , model: any
        , cleanBodyFunct: (value: any)=> InterfaceModel
        , formSchema: TypeFormSchema
    ): Promise<NotificationFinalStruct> {
        let res: NotificationStruct;
        try {
            value = initBodyFunct(value);
            console.log('----> _update > value:: ', value);
            if(ValidForm(formSchema, value)) {
                res = await this._exec_update<InterfaceBody, InterfaceModel>(
                    GetValidForm(formSchema, value)
                    , getIdsFunct
                    , findCondFunct
                    , model
                    , cleanBodyFunct
                );
            } else {
                res = this.notifications.invalid_data;
            }
        } catch (error) {
            console.log('----> _update > error:: ', error);
            res = this.notifications.unknown_error;
        }

        return {
            type: res.type,
            message: res.messages[this._lang],
        };
    }
    public async _exec_update<InterfaceBody, InterfaceModel>(
        value: any
        , getIdsFunct: (value: InterfaceBody) => any
        , findCondFunct: (ids: any)=> any
        , model: any
        , cleanBodyFunct: (value: any)=> InterfaceModel
    ): Promise<NotificationStruct> {
        let res: NotificationStruct;
        const ids = getIdsFunct(value);
        const findCond = findCondFunct(ids);
        const elementExists: number = await model.count({
            where: findCond,
        });
        if (elementExists > 0) {
            delete value.date_added;
            const cleanBody: InterfaceModel = cleanBodyFunct(
                GetBodyForUpdate(
                    (await model.findOne({
                        where: findCond,
                    })).dataValues
                    , value
                    , this.nullablePropertiesForUpdate
                )
            );
            console.log('----> _update > value:: ', value);
            console.log('----> _update > ids:: ', ids);
            console.log('----> _update > cleanBody:: ', cleanBody);
            console.log('----> _update > findCond:: ', findCond);
            console.log('----> _update > elementExists:: ', elementExists);
            await model.update(cleanBody, {
                where: findCond,
            });
            res = this.notifications.good_update;
        } else {
            res = this.notifications.not_found_data;
        }

        return res;
    }
}