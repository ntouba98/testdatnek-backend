import express from 'express';
import { Request, Response, NextFunction } from 'express';
import TestsFeature from '../features/tests.feature';

const router = express.Router();
const ElementFeature = TestsFeature;

router.get('/test1/', (req: Request, res: Response) => {
    const elementFeature = new ElementFeature();
    res.send(elementFeature.Test1());
});

export default router;