import { Module } from './interfaces/struct.interface';
import TestsRoutes from './routes/tests.routes';

const mainModule: Module[] = [
    {
        url: '/tests/',
        router: TestsRoutes,
    },
];
export default mainModule;