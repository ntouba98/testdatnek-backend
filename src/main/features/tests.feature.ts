import { Email } from "../scripts/communication";
import { AES, GenerateToken, MD5, SHA } from "../scripts/crypto";
import { GetCurrentDate, GetCustomTime, GetCustomTime2 } from "../scripts/date";
import { RemovePointer } from "../scripts/json";
import { GetCustomMonetaryAmount } from "../scripts/number";
import { Capitalize, Lower, RandomF, UcFirst, Upper } from "../scripts/string";


export default class TestsFeature {
    Test1(){
        /* await new Email().SendEmail('kidhell2016@gmail.com', `TEST | ${GetCurrentDate()} - ${RandomF()}`, `description - TEST | 21-11-2021 - ${RandomF()}`).catch((err) => {
            console.log(err);
        }); */
        return {
            value: 'ma fonction de test 1',
            rand_value: RandomF(50, 'alphanumeric_s', (data:string) => `rand_value-${data}`),
            uc_first_value: UcFirst('bilong ntouba celestin'),
            capitalize_value: Capitalize('bilong ntouba celestin'),
            lowercase_value: Lower('bilong ntouba celestin'),
            uppercase_value: Upper('bilong ntouba celestin'),
            dict_no_pointer: RemovePointer({
                nom: 'bilong ntouba',
                prenom: 'celestin',
            }),
            date_value: {
                custom_time: GetCustomTime('2021/11/24', 'fr'),
                custom_time2: GetCustomTime2('2021/11/24', 'fr'),
                current_date: GetCurrentDate(),
            },
            number_value: {
                money: GetCustomMonetaryAmount(2500000, 'fr', false),
            },
            crypto: {
                hash: {
                    sha_base64: new SHA().Hash('bilong', 512, 'base64'),
                    sha_hex: new SHA().Hash('bilong', 512, 'hex'),
                    md5_base64: new MD5().Hash('bilong', 'base64'),
                    md5_hex: new MD5().Hash('bilong', 'hex'),
                },
                cipher: {
                    aes: {
                        encrypt: new AES().Encrypt('bilong'),
                        decrypt: new AES().Decrypt(
                            new AES().Encrypt('bilong', 'utf8')
                        ),
                    }
                },
                token: GenerateToken({
                    id: 1337,
                    username: "john.doe",
                })
            }
        };
    }
}