import * as Sequelize from 'sequelize';
import { DataTypes, Model, Optional } from 'sequelize';

export interface view_auth_userAttributes {
  code: string;
  email?: string;
  username?: string;
  last_username?: string;
  password?: string;
  account_information?: object;
  date_joined: Date;
  date_updated: Date;
  status?: string;
  date_added_activation_key: Date;
  date_expired_activation_key?: Date;
  activation_key?: string;
  activation_key_enter?: string;
  is_activated?: number;
  group?: string;
  group_json?: object;
  permissions?: object;
  permissions_json?: object;
  demands_reset_password?: object;
  last_demand_reset_password?: object;
  all_connections?: object;
  last_connection?: object;
  activities?: object;
}

export type view_auth_userOptionalAttributes = "email" | "username" | "last_username" | "password" | "account_information" | "date_joined" | "date_updated" | "status" | "date_added_activation_key" | "date_expired_activation_key" | "activation_key" | "activation_key_enter" | "is_activated" | "group" | "group_json" | "permissions" | "permissions_json" | "demands_reset_password" | "last_demand_reset_password" | "all_connections" | "last_connection" | "activities";
export type view_auth_userCreationAttributes = Optional<view_auth_userAttributes, view_auth_userOptionalAttributes>;

export class view_auth_user extends Model<view_auth_userAttributes, view_auth_userCreationAttributes> implements view_auth_userAttributes {
  code!: string;
  email?: string;
  username?: string;
  last_username?: string;
  password?: string;
  account_information?: object;
  date_joined!: Date;
  date_updated!: Date;
  status?: string;
  date_added_activation_key!: Date;
  date_expired_activation_key?: Date;
  activation_key?: string;
  activation_key_enter?: string;
  is_activated?: number;
  group?: string;
  group_json?: object;
  permissions?: object;
  permissions_json?: object;
  demands_reset_password?: object;
  last_demand_reset_password?: object;
  all_connections?: object;
  last_connection?: object;
  activities?: object;


  static initModel(sequelize: Sequelize.Sequelize): typeof view_auth_user {
    view_auth_user.init({
    code: {
      type: DataTypes.STRING(30),
      allowNull: false
    },
    email: {
      type: DataTypes.STRING(254),
      allowNull: true
    },
    username: {
      type: DataTypes.STRING(150),
      allowNull: true
    },
    last_username: {
      type: DataTypes.STRING(150),
      allowNull: true
    },
    password: {
      type: DataTypes.STRING(128),
      allowNull: true
    },
    account_information: {
      type: DataTypes.JSON,
      allowNull: true
    },
    date_joined: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: "0000-00-00 00:00:00"
    },
    date_updated: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: "0000-00-00 00:00:00"
    },
    status: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    date_added_activation_key: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: "0000-00-00 00:00:00"
    },
    date_expired_activation_key: {
      type: DataTypes.DATE,
      allowNull: true
    },
    activation_key: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    activation_key_enter: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    is_activated: {
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    group: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    group_json: {
      type: DataTypes.JSON,
      allowNull: true
    },
    permissions: {
      type: DataTypes.JSON,
      allowNull: true
    },
    permissions_json: {
      type: DataTypes.JSON,
      allowNull: true
    },
    demands_reset_password: {
      type: DataTypes.JSON,
      allowNull: true
    },
    last_demand_reset_password: {
      type: DataTypes.JSON,
      allowNull: true
    },
    all_connections: {
      type: DataTypes.JSON,
      allowNull: true
    },
    last_connection: {
      type: DataTypes.JSON,
      allowNull: true
    },
    activities: {
      type: DataTypes.JSON,
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'view_auth_user',
    timestamps: false
  });
  return view_auth_user;
  }
}
