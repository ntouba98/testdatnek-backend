import * as Sequelize from 'sequelize';
import { DataTypes, Model, Optional } from 'sequelize';

export interface auth_user_has_groupAttributes {
  code: string;
  user_code: string;
  group_code: string;
  status: 'visible' | 'archived' | 'blocked';
}

export type auth_user_has_groupPk = "code";
export type auth_user_has_groupId = auth_user_has_group[auth_user_has_groupPk];
export type auth_user_has_groupOptionalAttributes = "code" | "status";
export type auth_user_has_groupCreationAttributes = Optional<auth_user_has_groupAttributes, auth_user_has_groupOptionalAttributes>;

export class auth_user_has_group extends Model<auth_user_has_groupAttributes, auth_user_has_groupCreationAttributes> implements auth_user_has_groupAttributes {
  code!: string;
  user_code!: string;
  group_code!: string;
  status!: 'visible' | 'archived' | 'blocked';


  static initModel(sequelize: Sequelize.Sequelize): typeof auth_user_has_group {
    auth_user_has_group.init({
    code: {
      type: DataTypes.STRING(30),
      allowNull: false,
      primaryKey: true
    },
    user_code: {
      type: DataTypes.STRING(30),
      allowNull: false
    },
    group_code: {
      type: DataTypes.STRING(30),
      allowNull: false
    },
    status: {
      type: DataTypes.ENUM('visible','archived','blocked'),
      allowNull: false,
      defaultValue: "visible"
    }
  }, {
    sequelize,
    tableName: 'auth_user_has_group',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "code" },
        ]
      },
    ]
  });
  return auth_user_has_group;
  }
}
