import * as Sequelize from 'sequelize';
import { DataTypes, Model, Optional } from 'sequelize';

export interface auth_reset_passwordAttributes {
  code: string;
  key_initial: string;
  key_enter: string;
  runtime: number;
  blocked: number;
  date_added: Date;
  user_code: string;
}

export type auth_reset_passwordPk = "code";
export type auth_reset_passwordId = auth_reset_password[auth_reset_passwordPk];
export type auth_reset_passwordOptionalAttributes = "code" | "runtime" | "blocked" | "date_added";
export type auth_reset_passwordCreationAttributes = Optional<auth_reset_passwordAttributes, auth_reset_passwordOptionalAttributes>;

export class auth_reset_password extends Model<auth_reset_passwordAttributes, auth_reset_passwordCreationAttributes> implements auth_reset_passwordAttributes {
  code!: string;
  key_initial!: string;
  key_enter!: string;
  runtime!: number;
  blocked!: number;
  date_added!: Date;
  user_code!: string;


  static initModel(sequelize: Sequelize.Sequelize): typeof auth_reset_password {
    auth_reset_password.init({
    code: {
      type: DataTypes.STRING(30),
      allowNull: false,
      primaryKey: true
    },
    key_initial: {
      type: DataTypes.TEXT,
      allowNull: false
    },
    key_enter: {
      type: DataTypes.TEXT,
      allowNull: false
    },
    runtime: {
      type: DataTypes.DOUBLE,
      allowNull: false,
      defaultValue: 1800
    },
    blocked: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: 0
    },
    date_added: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: Sequelize.Sequelize.literal('CURRENT_TIMESTAMP')
    },
    user_code: {
      type: DataTypes.STRING(30),
      allowNull: false
    }
  }, {
    sequelize,
    tableName: 'auth_reset_password',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "code" },
        ]
      },
    ]
  });
  return auth_reset_password;
  }
}
