import * as Sequelize from 'sequelize';
import { DataTypes, Model, Optional } from 'sequelize';

export interface view_auth_reset_passwordAttributes {
  code: string;
  key_initial: string;
  key_enter: string;
  runtime: number;
  blocked: number;
  date_added: Date;
  user_code: string;
  is_authorized?: number;
  date_expired?: Date;
}

export type view_auth_reset_passwordOptionalAttributes = "runtime" | "blocked" | "date_added" | "is_authorized" | "date_expired";
export type view_auth_reset_passwordCreationAttributes = Optional<view_auth_reset_passwordAttributes, view_auth_reset_passwordOptionalAttributes>;

export class view_auth_reset_password extends Model<view_auth_reset_passwordAttributes, view_auth_reset_passwordCreationAttributes> implements view_auth_reset_passwordAttributes {
  code!: string;
  key_initial!: string;
  key_enter!: string;
  runtime!: number;
  blocked!: number;
  date_added!: Date;
  user_code!: string;
  is_authorized?: number;
  date_expired?: Date;


  static initModel(sequelize: Sequelize.Sequelize): typeof view_auth_reset_password {
    view_auth_reset_password.init({
    code: {
      type: DataTypes.STRING(30),
      allowNull: false
    },
    key_initial: {
      type: DataTypes.TEXT,
      allowNull: false
    },
    key_enter: {
      type: DataTypes.TEXT,
      allowNull: false
    },
    runtime: {
      type: DataTypes.DOUBLE,
      allowNull: false,
      defaultValue: 1800
    },
    blocked: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: 0
    },
    date_added: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: "0000-00-00 00:00:00"
    },
    user_code: {
      type: DataTypes.STRING(30),
      allowNull: false
    },
    is_authorized: {
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    date_expired: {
      type: DataTypes.DATE,
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'view_auth_reset_password',
    timestamps: false
  });
  return view_auth_reset_password;
  }
}
