import * as Sequelize from 'sequelize';
import { DataTypes, Model, Optional } from 'sequelize';

export interface auth_activityAttributes {
  code: string;
  datas?: object;
  object: string;
  content: string;
  date_added: Date;
  author_code: string;
}

export type auth_activityPk = "code";
export type auth_activityId = auth_activity[auth_activityPk];
export type auth_activityOptionalAttributes = "code" | "datas" | "date_added";
export type auth_activityCreationAttributes = Optional<auth_activityAttributes, auth_activityOptionalAttributes>;

export class auth_activity extends Model<auth_activityAttributes, auth_activityCreationAttributes> implements auth_activityAttributes {
  code!: string;
  datas?: object;
  object!: string;
  content!: string;
  date_added!: Date;
  author_code!: string;


  static initModel(sequelize: Sequelize.Sequelize): typeof auth_activity {
    auth_activity.init({
    code: {
      type: DataTypes.STRING(30),
      allowNull: false,
      primaryKey: true
    },
    datas: {
      type: DataTypes.JSON,
      allowNull: true
    },
    object: {
      type: DataTypes.STRING(50),
      allowNull: false
    },
    content: {
      type: DataTypes.TEXT,
      allowNull: false
    },
    date_added: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: Sequelize.Sequelize.literal('CURRENT_TIMESTAMP')
    },
    author_code: {
      type: DataTypes.STRING(30),
      allowNull: false
    }
  }, {
    sequelize,
    tableName: 'auth_activity',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "code" },
        ]
      },
    ]
  });
  return auth_activity;
  }
}
