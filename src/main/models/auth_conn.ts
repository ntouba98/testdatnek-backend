import * as Sequelize from 'sequelize';
import { DataTypes, Model, Optional } from 'sequelize';

export interface auth_connAttributes {
  code: string;
  user_datas: object;
  token: string;
  key_initial?: string;
  runtime: number;
  blocked: number;
  first_conn: Date;
  last_conn: Date;
  user_code: string;
}

export type auth_connPk = "code";
export type auth_connId = auth_conn[auth_connPk];
export type auth_connOptionalAttributes = "code" | "key_initial" | "runtime" | "blocked" | "first_conn" | "last_conn";
export type auth_connCreationAttributes = Optional<auth_connAttributes, auth_connOptionalAttributes>;

export class auth_conn extends Model<auth_connAttributes, auth_connCreationAttributes> implements auth_connAttributes {
  code!: string;
  user_datas!: object;
  token!: string;
  key_initial?: string;
  runtime!: number;
  blocked!: number;
  first_conn!: Date;
  last_conn!: Date;
  user_code!: string;


  static initModel(sequelize: Sequelize.Sequelize): typeof auth_conn {
    auth_conn.init({
    code: {
      type: DataTypes.STRING(30),
      allowNull: false,
      primaryKey: true
    },
    user_datas: {
      type: DataTypes.JSON,
      allowNull: false
    },
    token: {
      type: DataTypes.STRING(100),
      allowNull: false,
      unique: "token"
    },
    key_initial: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    runtime: {
      type: DataTypes.DOUBLE,
      allowNull: false,
      defaultValue: 86400
    },
    blocked: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: 0
    },
    first_conn: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: Sequelize.Sequelize.literal('CURRENT_TIMESTAMP')
    },
    last_conn: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: Sequelize.Sequelize.literal('CURRENT_TIMESTAMP')
    },
    user_code: {
      type: DataTypes.STRING(30),
      allowNull: false
    }
  }, {
    sequelize,
    tableName: 'auth_conn',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "code" },
        ]
      },
      {
        name: "token",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "token" },
        ]
      },
    ]
  });
  return auth_conn;
  }
}
