import * as Sequelize from 'sequelize';
import { DataTypes, Model, Optional } from 'sequelize';

export interface auth_user_subscriptionAttributes {
  code: string;
  type: 'all';
  user_code: string;
  subscriber_code: string;
  status: 'visible' | 'archived' | 'blocked';
  date_added: Date;
  date_updated: Date;
}

export type auth_user_subscriptionPk = "code";
export type auth_user_subscriptionId = auth_user_subscription[auth_user_subscriptionPk];
export type auth_user_subscriptionOptionalAttributes = "code" | "type" | "status" | "date_added" | "date_updated";
export type auth_user_subscriptionCreationAttributes = Optional<auth_user_subscriptionAttributes, auth_user_subscriptionOptionalAttributes>;

export class auth_user_subscription extends Model<auth_user_subscriptionAttributes, auth_user_subscriptionCreationAttributes> implements auth_user_subscriptionAttributes {
  code!: string;
  type!: 'all';
  user_code!: string;
  subscriber_code!: string;
  status!: 'visible' | 'archived' | 'blocked';
  date_added!: Date;
  date_updated!: Date;


  static initModel(sequelize: Sequelize.Sequelize): typeof auth_user_subscription {
    auth_user_subscription.init({
    code: {
      type: DataTypes.STRING(30),
      allowNull: false,
      primaryKey: true
    },
    type: {
      type: DataTypes.ENUM('all'),
      allowNull: false,
      defaultValue: "all"
    },
    user_code: {
      type: DataTypes.STRING(30),
      allowNull: false
    },
    subscriber_code: {
      type: DataTypes.STRING(30),
      allowNull: false
    },
    status: {
      type: DataTypes.ENUM('visible','archived','blocked'),
      allowNull: false,
      defaultValue: "visible"
    },
    date_added: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: Sequelize.Sequelize.literal('CURRENT_TIMESTAMP')
    },
    date_updated: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: Sequelize.Sequelize.literal('CURRENT_TIMESTAMP')
    }
  }, {
    sequelize,
    tableName: 'auth_user_subscription',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "code" },
        ]
      },
    ]
  });
  return auth_user_subscription;
  }
}
