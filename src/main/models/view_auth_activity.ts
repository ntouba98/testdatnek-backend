import * as Sequelize from 'sequelize';
import { DataTypes, Model, Optional } from 'sequelize';

export interface view_auth_activityAttributes {
  code: string;
  datas?: object;
  object: string;
  content: string;
  date_added: Date;
  author_code: string;
  author_username?: string;
  account_information?: object;
}

export type view_auth_activityOptionalAttributes = "datas" | "date_added" | "author_username" | "account_information";
export type view_auth_activityCreationAttributes = Optional<view_auth_activityAttributes, view_auth_activityOptionalAttributes>;

export class view_auth_activity extends Model<view_auth_activityAttributes, view_auth_activityCreationAttributes> implements view_auth_activityAttributes {
  code!: string;
  datas?: object;
  object!: string;
  content!: string;
  date_added!: Date;
  author_code!: string;
  author_username?: string;
  account_information?: object;


  static initModel(sequelize: Sequelize.Sequelize): typeof view_auth_activity {
    view_auth_activity.init({
    code: {
      type: DataTypes.STRING(30),
      allowNull: false
    },
    datas: {
      type: DataTypes.JSON,
      allowNull: true
    },
    object: {
      type: DataTypes.STRING(50),
      allowNull: false
    },
    content: {
      type: DataTypes.TEXT,
      allowNull: false
    },
    date_added: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: "0000-00-00 00:00:00"
    },
    author_code: {
      type: DataTypes.STRING(30),
      allowNull: false
    },
    author_username: {
      type: DataTypes.STRING(150),
      allowNull: true
    },
    account_information: {
      type: DataTypes.JSON,
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'view_auth_activity',
    timestamps: false
  });
  return view_auth_activity;
  }
}
