import * as Sequelize from 'sequelize';
import { DataTypes, Model, Optional } from 'sequelize';

export interface auth_group_has_permissionAttributes {
  code: string;
  group_code: string;
  permission_code: string;
  status: 'visible' | 'archived' | 'blocked';
}

export type auth_group_has_permissionPk = "code";
export type auth_group_has_permissionId = auth_group_has_permission[auth_group_has_permissionPk];
export type auth_group_has_permissionOptionalAttributes = "code" | "status";
export type auth_group_has_permissionCreationAttributes = Optional<auth_group_has_permissionAttributes, auth_group_has_permissionOptionalAttributes>;

export class auth_group_has_permission extends Model<auth_group_has_permissionAttributes, auth_group_has_permissionCreationAttributes> implements auth_group_has_permissionAttributes {
  code!: string;
  group_code!: string;
  permission_code!: string;
  status!: 'visible' | 'archived' | 'blocked';


  static initModel(sequelize: Sequelize.Sequelize): typeof auth_group_has_permission {
    auth_group_has_permission.init({
    code: {
      type: DataTypes.STRING(30),
      allowNull: false,
      primaryKey: true
    },
    group_code: {
      type: DataTypes.STRING(30),
      allowNull: false
    },
    permission_code: {
      type: DataTypes.STRING(30),
      allowNull: false
    },
    status: {
      type: DataTypes.ENUM('visible','archived','blocked'),
      allowNull: false,
      defaultValue: "visible"
    }
  }, {
    sequelize,
    tableName: 'auth_group_has_permission',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "code" },
        ]
      },
    ]
  });
  return auth_group_has_permission;
  }
}
