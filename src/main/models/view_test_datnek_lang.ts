import * as Sequelize from 'sequelize';
import { DataTypes, Model, Optional } from 'sequelize';

export interface view_test_datnek_langAttributes {
  code: string;
  lang: 'en' | 'fr' | 'nl';
  spoken_level: 'intermediate' | 'pre_intermediate' | 'current' | 'elementary';
  written_level: 'intermediate' | 'pre_intermediate' | 'current' | 'elementary';
  understanding_level: 'intermediate' | 'pre_intermediate' | 'current' | 'elementary';
  date_added: Date;
  date_updated: Date;
  status: 'visible' | 'archived' | 'blocked';
}

export type view_test_datnek_langOptionalAttributes = "date_added" | "date_updated" | "status";
export type view_test_datnek_langCreationAttributes = Optional<view_test_datnek_langAttributes, view_test_datnek_langOptionalAttributes>;

export class view_test_datnek_lang extends Model<view_test_datnek_langAttributes, view_test_datnek_langCreationAttributes> implements view_test_datnek_langAttributes {
  code!: string;
  lang!: 'en' | 'fr' | 'nl';
  spoken_level!: 'intermediate' | 'pre_intermediate' | 'current' | 'elementary';
  written_level!: 'intermediate' | 'pre_intermediate' | 'current' | 'elementary';
  understanding_level!: 'intermediate' | 'pre_intermediate' | 'current' | 'elementary';
  date_added!: Date;
  date_updated!: Date;
  status!: 'visible' | 'archived' | 'blocked';


  static initModel(sequelize: Sequelize.Sequelize): typeof view_test_datnek_lang {
    return view_test_datnek_lang.init({
    code: {
      type: DataTypes.STRING(30),
      allowNull: false
    },
    lang: {
      type: DataTypes.ENUM('en','fr','nl'),
      allowNull: false
    },
    spoken_level: {
      type: DataTypes.ENUM('intermediate','pre_intermediate','current','elementary'),
      allowNull: false
    },
    written_level: {
      type: DataTypes.ENUM('intermediate','pre_intermediate','current','elementary'),
      allowNull: false
    },
    understanding_level: {
      type: DataTypes.ENUM('intermediate','pre_intermediate','current','elementary'),
      allowNull: false
    },
    date_added: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: "0000-00-00 00:00:00"
    },
    date_updated: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: "0000-00-00 00:00:00"
    },
    status: {
      type: DataTypes.ENUM('visible','archived','blocked'),
      allowNull: false,
      defaultValue: "visible"
    }
  }, {
    sequelize,
    tableName: 'view_test_datnek_lang',
    timestamps: false
  });
  }
}
