import * as Sequelize from 'sequelize';
import { DataTypes, Model, Optional } from 'sequelize';

export interface view_auth_user_subscriptionAttributes {
  user_code: string;
  type: 'all';
  len: number;
  user?: object;
  username?: string;
  subscribers?: object;
  subscriber_names?: object;
}

export type view_auth_user_subscriptionOptionalAttributes = "type" | "len" | "user" | "username" | "subscribers" | "subscriber_names";
export type view_auth_user_subscriptionCreationAttributes = Optional<view_auth_user_subscriptionAttributes, view_auth_user_subscriptionOptionalAttributes>;

export class view_auth_user_subscription extends Model<view_auth_user_subscriptionAttributes, view_auth_user_subscriptionCreationAttributes> implements view_auth_user_subscriptionAttributes {
  user_code!: string;
  type!: 'all';
  len!: number;
  user?: object;
  username?: string;
  subscribers?: object;
  subscriber_names?: object;


  static initModel(sequelize: Sequelize.Sequelize): typeof view_auth_user_subscription {
    view_auth_user_subscription.init({
    user_code: {
      type: DataTypes.STRING(30),
      allowNull: false
    },
    type: {
      type: DataTypes.ENUM('all'),
      allowNull: false,
      defaultValue: "all"
    },
    len: {
      type: DataTypes.BIGINT,
      allowNull: false,
      defaultValue: 0
    },
    user: {
      type: DataTypes.JSON,
      allowNull: true
    },
    username: {
      type: DataTypes.STRING(150),
      allowNull: true
    },
    subscribers: {
      type: DataTypes.JSON,
      allowNull: true
    },
    subscriber_names: {
      type: DataTypes.JSON,
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'view_auth_user_subscription',
    timestamps: false
  });
  return view_auth_user_subscription;
  }
}
