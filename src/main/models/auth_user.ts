import * as Sequelize from 'sequelize';
import { DataTypes, Model, Optional } from 'sequelize';

export interface auth_userAttributes {
  code: string;
  email?: string;
  username?: string;
  last_username?: string;
  password?: string;
  account_information?: object;
  activation_key?: string;
  activation_key_enter?: string;
  date_added_activation_key: Date;
  date_joined: Date;
  date_updated: Date;
  status: 'visible' | 'archived' | 'blocked';
}

export type auth_userPk = "code";
export type auth_userId = auth_user[auth_userPk];
export type auth_userOptionalAttributes = "code" | "email" | "username" | "last_username" | "password" | "account_information" | "activation_key" | "activation_key_enter" | "date_added_activation_key" | "date_joined" | "date_updated" | "status";
export type auth_userCreationAttributes = Optional<auth_userAttributes, auth_userOptionalAttributes>;

export class auth_user extends Model<auth_userAttributes, auth_userCreationAttributes> implements auth_userAttributes {
  code!: string;
  email?: string;
  username?: string;
  last_username?: string;
  password?: string;
  account_information?: object;
  activation_key?: string;
  activation_key_enter?: string;
  date_added_activation_key!: Date;
  date_joined!: Date;
  date_updated!: Date;
  status!: 'visible' | 'archived' | 'blocked';


  static initModel(sequelize: Sequelize.Sequelize): typeof auth_user {
    auth_user.init({
    code: {
      type: DataTypes.STRING(30),
      allowNull: false,
      primaryKey: true
    },
    email: {
      type: DataTypes.STRING(254),
      allowNull: true,
      unique: "email"
    },
    username: {
      type: DataTypes.STRING(150),
      allowNull: true,
      unique: "username"
    },
    last_username: {
      type: DataTypes.STRING(150),
      allowNull: true
    },
    password: {
      type: DataTypes.STRING(128),
      allowNull: true
    },
    account_information: {
      type: DataTypes.JSON,
      allowNull: true
    },
    activation_key: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    activation_key_enter: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    date_added_activation_key: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: Sequelize.Sequelize.literal('CURRENT_TIMESTAMP')
    },
    date_joined: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: Sequelize.Sequelize.literal('CURRENT_TIMESTAMP')
    },
    date_updated: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: Sequelize.Sequelize.literal('CURRENT_TIMESTAMP')
    },
    status: {
      type: DataTypes.ENUM('visible','archived','blocked'),
      allowNull: false,
      defaultValue: "visible"
    }
  }, {
    sequelize,
    tableName: 'auth_user',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "code" },
        ]
      },
      {
        name: "email",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "email" },
        ]
      },
      {
        name: "username",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "username" },
        ]
      },
    ]
  });
  return auth_user;
  }
}
