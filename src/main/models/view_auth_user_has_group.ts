import * as Sequelize from 'sequelize';
import { DataTypes, Model, Optional } from 'sequelize';

export interface view_auth_user_has_groupAttributes {
  code: string;
  user_code: string;
  group_code: string;
  status: 'visible' | 'archived' | 'blocked';
  user?: object;
  group?: object;
}

export type view_auth_user_has_groupOptionalAttributes = "status" | "user" | "group";
export type view_auth_user_has_groupCreationAttributes = Optional<view_auth_user_has_groupAttributes, view_auth_user_has_groupOptionalAttributes>;

export class view_auth_user_has_group extends Model<view_auth_user_has_groupAttributes, view_auth_user_has_groupCreationAttributes> implements view_auth_user_has_groupAttributes {
  code!: string;
  user_code!: string;
  group_code!: string;
  status!: 'visible' | 'archived' | 'blocked';
  user?: object;
  group?: object;


  static initModel(sequelize: Sequelize.Sequelize): typeof view_auth_user_has_group {
    view_auth_user_has_group.init({
    code: {
      type: DataTypes.STRING(30),
      allowNull: false
    },
    user_code: {
      type: DataTypes.STRING(30),
      allowNull: false
    },
    group_code: {
      type: DataTypes.STRING(30),
      allowNull: false
    },
    status: {
      type: DataTypes.ENUM('visible','archived','blocked'),
      allowNull: false,
      defaultValue: "visible"
    },
    user: {
      type: DataTypes.JSON,
      allowNull: true
    },
    group: {
      type: DataTypes.JSON,
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'view_auth_user_has_group',
    timestamps: false
  });
  return view_auth_user_has_group;
  }
}
