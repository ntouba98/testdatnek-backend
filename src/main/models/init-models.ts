import type { Sequelize } from "sequelize";
import { test_datnek_lang as _test_datnek_lang } from "./test_datnek_lang";
import type { test_datnek_langAttributes, test_datnek_langCreationAttributes } from "./test_datnek_lang";
import { view_test_datnek_lang as _view_test_datnek_lang } from "./view_test_datnek_lang";
import type { view_test_datnek_langAttributes, view_test_datnek_langCreationAttributes } from "./view_test_datnek_lang";

export {
 _test_datnek_lang as test_datnek_lang,
 _view_test_datnek_lang as view_test_datnek_lang,
};

export type {
  test_datnek_langAttributes,
  test_datnek_langCreationAttributes,
  view_test_datnek_langAttributes,
  view_test_datnek_langCreationAttributes,
};

export function initModels(sequelize: Sequelize) {
  const test_datnek_lang = _test_datnek_lang.initModel(sequelize);
  const view_test_datnek_lang = _view_test_datnek_lang.initModel(sequelize);


  return {
    test_datnek_lang: test_datnek_lang,
    view_test_datnek_lang: view_test_datnek_lang,
  };
}
