import * as Sequelize from 'sequelize';
import { DataTypes, Model, Optional } from 'sequelize';

export interface view_auth_connAttributes {
  code: string;
  user_datas: object;
  token: string;
  key_initial?: string;
  runtime: number;
  blocked: number;
  first_conn: Date;
  last_conn: Date;
  user_code: string;
  conn_expired?: Date;
  user?: object;
  author?: string;
}

export type view_auth_connOptionalAttributes = "key_initial" | "runtime" | "blocked" | "first_conn" | "last_conn" | "conn_expired" | "user" | "author";
export type view_auth_connCreationAttributes = Optional<view_auth_connAttributes, view_auth_connOptionalAttributes>;

export class view_auth_conn extends Model<view_auth_connAttributes, view_auth_connCreationAttributes> implements view_auth_connAttributes {
  code!: string;
  user_datas!: object;
  token!: string;
  key_initial?: string;
  runtime!: number;
  blocked!: number;
  first_conn!: Date;
  last_conn!: Date;
  user_code!: string;
  conn_expired?: Date;
  user?: object;
  author?: string;


  static initModel(sequelize: Sequelize.Sequelize): typeof view_auth_conn {
    view_auth_conn.init({
    code: {
      type: DataTypes.STRING(30),
      allowNull: false
    },
    user_datas: {
      type: DataTypes.JSON,
      allowNull: false
    },
    token: {
      type: DataTypes.STRING(100),
      allowNull: false
    },
    key_initial: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    runtime: {
      type: DataTypes.DOUBLE,
      allowNull: false,
      defaultValue: 86400
    },
    blocked: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: 0
    },
    first_conn: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: "0000-00-00 00:00:00"
    },
    last_conn: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: "0000-00-00 00:00:00"
    },
    user_code: {
      type: DataTypes.STRING(30),
      allowNull: false
    },
    conn_expired: {
      type: DataTypes.DATE,
      allowNull: true
    },
    user: {
      type: DataTypes.JSON,
      allowNull: true
    },
    author: {
      type: DataTypes.STRING(150),
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'view_auth_conn',
    timestamps: false
  });
  return view_auth_conn;
  }
}
