import * as Sequelize from 'sequelize';
import { DataTypes, Model, Optional } from 'sequelize';

export interface auth_permissionAttributes {
  code: string;
  name: string;
  date_added: Date;
  date_updated: Date;
  status: 'visible' | 'archived' | 'blocked';
}

export type auth_permissionPk = "code";
export type auth_permissionId = auth_permission[auth_permissionPk];
export type auth_permissionOptionalAttributes = "code" | "date_added" | "date_updated" | "status";
export type auth_permissionCreationAttributes = Optional<auth_permissionAttributes, auth_permissionOptionalAttributes>;

export class auth_permission extends Model<auth_permissionAttributes, auth_permissionCreationAttributes> implements auth_permissionAttributes {
  code!: string;
  name!: string;
  date_added!: Date;
  date_updated!: Date;
  status!: 'visible' | 'archived' | 'blocked';


  static initModel(sequelize: Sequelize.Sequelize): typeof auth_permission {
    auth_permission.init({
    code: {
      type: DataTypes.STRING(30),
      allowNull: false,
      primaryKey: true
    },
    name: {
      type: DataTypes.STRING(50),
      allowNull: false,
      unique: "name"
    },
    date_added: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: Sequelize.Sequelize.literal('CURRENT_TIMESTAMP')
    },
    date_updated: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: Sequelize.Sequelize.literal('CURRENT_TIMESTAMP')
    },
    status: {
      type: DataTypes.ENUM('visible','archived','blocked'),
      allowNull: false,
      defaultValue: "visible"
    }
  }, {
    sequelize,
    tableName: 'auth_permission',
    hasTrigger: true,
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "code" },
        ]
      },
      {
        name: "name",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "name" },
        ]
      },
    ]
  });
  return auth_permission;
  }
}
