import * as Sequelize from 'sequelize';
import { DataTypes, Model, Optional } from 'sequelize';

export interface view_auth_user_subscription_statAttributes {
  user_code: string;
  type: 'all';
  len: number;
}

export type view_auth_user_subscription_statOptionalAttributes = "type" | "len";
export type view_auth_user_subscription_statCreationAttributes = Optional<view_auth_user_subscription_statAttributes, view_auth_user_subscription_statOptionalAttributes>;

export class view_auth_user_subscription_stat extends Model<view_auth_user_subscription_statAttributes, view_auth_user_subscription_statCreationAttributes> implements view_auth_user_subscription_statAttributes {
  user_code!: string;
  type!: 'all';
  len!: number;


  static initModel(sequelize: Sequelize.Sequelize): typeof view_auth_user_subscription_stat {
    view_auth_user_subscription_stat.init({
    user_code: {
      type: DataTypes.STRING(30),
      allowNull: false
    },
    type: {
      type: DataTypes.ENUM('all'),
      allowNull: false,
      defaultValue: "all"
    },
    len: {
      type: DataTypes.BIGINT,
      allowNull: false,
      defaultValue: 0
    }
  }, {
    sequelize,
    tableName: 'view_auth_user_subscription_stat',
    timestamps: false
  });
  return view_auth_user_subscription_stat;
  }
}
