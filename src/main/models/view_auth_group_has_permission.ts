import * as Sequelize from 'sequelize';
import { DataTypes, Model, Optional } from 'sequelize';

export interface view_auth_group_has_permissionAttributes {
  code: string;
  group_code: string;
  permission_code: string;
  status: 'visible' | 'archived' | 'blocked';
  group?: object;
}

export type view_auth_group_has_permissionOptionalAttributes = "status" | "group";
export type view_auth_group_has_permissionCreationAttributes = Optional<view_auth_group_has_permissionAttributes, view_auth_group_has_permissionOptionalAttributes>;

export class view_auth_group_has_permission extends Model<view_auth_group_has_permissionAttributes, view_auth_group_has_permissionCreationAttributes> implements view_auth_group_has_permissionAttributes {
  code!: string;
  group_code!: string;
  permission_code!: string;
  status!: 'visible' | 'archived' | 'blocked';
  group?: object;


  static initModel(sequelize: Sequelize.Sequelize): typeof view_auth_group_has_permission {
    view_auth_group_has_permission.init({
    code: {
      type: DataTypes.STRING(30),
      allowNull: false
    },
    group_code: {
      type: DataTypes.STRING(30),
      allowNull: false
    },
    permission_code: {
      type: DataTypes.STRING(30),
      allowNull: false
    },
    status: {
      type: DataTypes.ENUM('visible','archived','blocked'),
      allowNull: false,
      defaultValue: "visible"
    },
    group: {
      type: DataTypes.JSON,
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'view_auth_group_has_permission',
    timestamps: false
  });
  return view_auth_group_has_permission;
  }
}
