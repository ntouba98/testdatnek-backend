import * as Sequelize from 'sequelize';
import { DataTypes, Model, Optional } from 'sequelize';

export interface view_auth_user_simplifiedAttributes {
  code: string;
  email?: string;
  username?: string;
  last_username?: string;
  password?: string;
  account_information?: object;
  date_joined: Date;
  date_updated: Date;
  status?: string;
  group?: string;
  group_json?: object;
}

export type view_auth_user_simplifiedOptionalAttributes = "email" | "username" | "last_username" | "password" | "account_information" | "date_joined" | "date_updated" | "status" | "group" | "group_json";
export type view_auth_user_simplifiedCreationAttributes = Optional<view_auth_user_simplifiedAttributes, view_auth_user_simplifiedOptionalAttributes>;

export class view_auth_user_simplified extends Model<view_auth_user_simplifiedAttributes, view_auth_user_simplifiedCreationAttributes> implements view_auth_user_simplifiedAttributes {
  code!: string;
  email?: string;
  username?: string;
  last_username?: string;
  password?: string;
  account_information?: object;
  date_joined!: Date;
  date_updated!: Date;
  status?: string;
  group?: string;
  group_json?: object;


  static initModel(sequelize: Sequelize.Sequelize): typeof view_auth_user_simplified {
    view_auth_user_simplified.init({
    code: {
      type: DataTypes.STRING(30),
      allowNull: false
    },
    email: {
      type: DataTypes.STRING(254),
      allowNull: true
    },
    username: {
      type: DataTypes.STRING(150),
      allowNull: true
    },
    last_username: {
      type: DataTypes.STRING(150),
      allowNull: true
    },
    password: {
      type: DataTypes.STRING(128),
      allowNull: true
    },
    account_information: {
      type: DataTypes.JSON,
      allowNull: true
    },
    date_joined: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: "0000-00-00 00:00:00"
    },
    date_updated: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: "0000-00-00 00:00:00"
    },
    status: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    group: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    group_json: {
      type: DataTypes.JSON,
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'view_auth_user_simplified',
    timestamps: false
  });
  return view_auth_user_simplified;
  }
}
