import * as Sequelize from 'sequelize';
import { DataTypes, Model, Optional } from 'sequelize';

export interface auth_groupAttributes {
  code: string;
  name: string;
  priority?: string;
  date_added: Date;
  date_updated: Date;
  status: 'visible' | 'archived' | 'blocked';
}

export type auth_groupPk = "code";
export type auth_groupId = auth_group[auth_groupPk];
export type auth_groupOptionalAttributes = "code" | "priority" | "date_added" | "date_updated" | "status";
export type auth_groupCreationAttributes = Optional<auth_groupAttributes, auth_groupOptionalAttributes>;

export class auth_group extends Model<auth_groupAttributes, auth_groupCreationAttributes> implements auth_groupAttributes {
  code!: string;
  name!: string;
  priority?: string;
  date_added!: Date;
  date_updated!: Date;
  status!: 'visible' | 'archived' | 'blocked';


  static initModel(sequelize: Sequelize.Sequelize): typeof auth_group {
    auth_group.init({
    code: {
      type: DataTypes.STRING(30),
      allowNull: false,
      primaryKey: true
    },
    name: {
      type: DataTypes.STRING(50),
      allowNull: false,
      unique: "name"
    },
    priority: {
      type: DataTypes.STRING(30),
      allowNull: true,
      defaultValue: "0"
    },
    date_added: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: Sequelize.Sequelize.literal('CURRENT_TIMESTAMP')
    },
    date_updated: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: Sequelize.Sequelize.literal('CURRENT_TIMESTAMP')
    },
    status: {
      type: DataTypes.ENUM('visible','archived','blocked'),
      allowNull: false,
      defaultValue: "visible"
    }
  }, {
    sequelize,
    tableName: 'auth_group',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "code" },
        ]
      },
      {
        name: "name",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "name" },
        ]
      },
    ]
  });
  return auth_group;
  }
}
