import * as Sequelize from 'sequelize';
import { DataTypes, Model, Optional } from 'sequelize';

export interface test_datnek_langAttributes {
  code: string;
  lang: 'en' | 'fr' | 'nl';
  spoken_level: 'intermediate' | 'pre_intermediate' | 'current' | 'elementary';
  written_level: 'intermediate' | 'pre_intermediate' | 'current' | 'elementary';
  understanding_level: 'intermediate' | 'pre_intermediate' | 'current' | 'elementary';
  date_added: Date;
  date_updated: Date;
  status: 'visible' | 'archived' | 'blocked';
}

export type test_datnek_langPk = "code";
export type test_datnek_langId = test_datnek_lang[test_datnek_langPk];
export type test_datnek_langOptionalAttributes = "code" | "date_added" | "date_updated" | "status";
export type test_datnek_langCreationAttributes = Optional<test_datnek_langAttributes, test_datnek_langOptionalAttributes>;

export class test_datnek_lang extends Model<test_datnek_langAttributes, test_datnek_langCreationAttributes> implements test_datnek_langAttributes {
  code!: string;
  lang!: 'en' | 'fr' | 'nl';
  spoken_level!: 'intermediate' | 'pre_intermediate' | 'current' | 'elementary';
  written_level!: 'intermediate' | 'pre_intermediate' | 'current' | 'elementary';
  understanding_level!: 'intermediate' | 'pre_intermediate' | 'current' | 'elementary';
  date_added!: Date;
  date_updated!: Date;
  status!: 'visible' | 'archived' | 'blocked';


  static initModel(sequelize: Sequelize.Sequelize): typeof test_datnek_lang {
    return test_datnek_lang.init({
    code: {
      type: DataTypes.STRING(30),
      allowNull: false,
      primaryKey: true
    },
    lang: {
      type: DataTypes.ENUM('en','fr','nl'),
      allowNull: false,
      unique: "lang"
    },
    spoken_level: {
      type: DataTypes.ENUM('intermediate','pre_intermediate','current','elementary'),
      allowNull: false
    },
    written_level: {
      type: DataTypes.ENUM('intermediate','pre_intermediate','current','elementary'),
      allowNull: false
    },
    understanding_level: {
      type: DataTypes.ENUM('intermediate','pre_intermediate','current','elementary'),
      allowNull: false
    },
    date_added: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: Sequelize.Sequelize.literal('CURRENT_TIMESTAMP')
    },
    date_updated: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: Sequelize.Sequelize.literal('CURRENT_TIMESTAMP')
    },
    status: {
      type: DataTypes.ENUM('visible','archived','blocked'),
      allowNull: false,
      defaultValue: "visible"
    }
  }, {
    sequelize,
    tableName: 'test_datnek_lang',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "code" },
        ]
      },
      {
        name: "lang",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "lang" },
        ]
      },
    ]
  });
  }
}
