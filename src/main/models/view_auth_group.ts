import * as Sequelize from 'sequelize';
import { DataTypes, Model, Optional } from 'sequelize';

export interface view_auth_groupAttributes {
  code: string;
  name: string;
  priority?: string;
  date_added: Date;
  date_updated: Date;
  status: 'visible' | 'archived' | 'blocked';
  permissions?: object;
}

export type view_auth_groupOptionalAttributes = "priority" | "date_added" | "date_updated" | "status" | "permissions";
export type view_auth_groupCreationAttributes = Optional<view_auth_groupAttributes, view_auth_groupOptionalAttributes>;

export class view_auth_group extends Model<view_auth_groupAttributes, view_auth_groupCreationAttributes> implements view_auth_groupAttributes {
  code!: string;
  name!: string;
  priority?: string;
  date_added!: Date;
  date_updated!: Date;
  status!: 'visible' | 'archived' | 'blocked';
  permissions?: object;


  static initModel(sequelize: Sequelize.Sequelize): typeof view_auth_group {
    view_auth_group.init({
    code: {
      type: DataTypes.STRING(30),
      allowNull: false
    },
    name: {
      type: DataTypes.STRING(50),
      allowNull: false
    },
    priority: {
      type: DataTypes.STRING(30),
      allowNull: true,
      defaultValue: "0"
    },
    date_added: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: "0000-00-00 00:00:00"
    },
    date_updated: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: "0000-00-00 00:00:00"
    },
    status: {
      type: DataTypes.ENUM('visible','archived','blocked'),
      allowNull: false,
      defaultValue: "visible"
    },
    permissions: {
      type: DataTypes.JSON,
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'view_auth_group',
    timestamps: false
  });
  return view_auth_group;
  }
}
