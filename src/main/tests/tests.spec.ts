import 'jasmine';
import TestsFeature from '../features/tests.feature';

const elementFeature = new TestsFeature();

describe("Test 1", function() {
    it("contains spec with an expectation", function() {
        const task = elementFeature.Test1();
        expect((
            typeof task === 'object'
            && Array.isArray(task) === false
        )).toBe(true);
    });
});