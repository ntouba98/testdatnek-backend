import Joi from 'joi';
import { langs, levelsLang, status } from '../../main/scripts/config';
import { ArrayToRegExp, RandomF } from '../../main/scripts/string';

export function StructListForm(datasStruct: any): any {
    return {
        datas: Joi.array().items(
            Joi.object(datasStruct)
        ),
        len: Joi.number().integer().required(),
        lenMax: Joi.number().integer().required(),
        exists: Joi.boolean(),
        page: Joi.number().integer().required(),
        pagePossible: Joi.array().items(
            Joi.number().integer().required()
        ),
        precPagePossible: Joi.number().integer().required(),
        nextPagePossible: Joi.number().integer().required(),
        pages: Joi.alternatives().try(
            Joi.number().integer().required(),
            Joi.string().valid('all').required()
        ),
        pagesPossible: Joi.array().items(
            Joi.alternatives().try(
                Joi.number().integer().required(),
                Joi.string().valid('all').required()
            )
        ),
        orderBy: Joi.string().required()
    }
}