import { Main } from './interfaces/struct.interface';
import MainModule from './main.module';
import TestDatnekModule from '../test-datnek/test-datnek.module';

let main: Main[] = [];
main = main.concat(
    MainModule,
    TestDatnekModule
);
export default main;