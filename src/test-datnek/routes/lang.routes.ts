import express from 'express';
import { Request, Response, NextFunction } from 'express';
import LangFeature from '../features/lang.feature';

const router = express.Router();
const ElementFeature = LangFeature;


router.get('/test', (req: Request, res: Response) => {
    res.send('la route fonctionne');
});

router.get('/', async (req: Request, res: Response) => {
    const elementFeature = new ElementFeature(String(req.query.lang));
    res.send(
        await elementFeature.FindAll(
            {
                status: req.query.status ? String(req.query.status) : undefined,
            }, parseFloat(String(req.query.page))
            , parseFloat(String(req.query.pages))
            , String(req.query.order_by)
        )
    );
});
router.get('/:id/exists', async (req: Request, res: Response) => {
    const elementFeature = new ElementFeature(String(req.query.lang));
    res.send(
        await elementFeature.Exists(req.params.id)
    );
});
router.get('/:id/', async (req: Request, res: Response) => {
    const elementFeature = new ElementFeature(String(req.query.lang));
    res.send(
        await elementFeature.Find(req.params.id)
    );
});

router.post('/add/', async (req: Request, res: Response) => {
    // url parameter > req.params.id
    // query parameter > req.query.lang
    // body > req.body
    // get header > req.headers.authorization
    const elementFeature = new ElementFeature(String(req.query.lang));
    res.send(
        await elementFeature.Add(req.body)
    );
});
router.put('/update/', async (req: Request, res: Response) => {
    const elementFeature = new ElementFeature(String(req.query.lang));
    res.send(
        await elementFeature.Update(req.body)
    );
});
router.post('/edit/', async (req: Request, res: Response) => {
    const elementFeature = new ElementFeature(String(req.query.lang));
    res.send(
        await elementFeature.Edit(req.body)
    );
});

router.delete('/archive/:id/', async (req: Request, res: Response) => {
    const elementFeature = new ElementFeature(String(req.query.lang));
    res.send(
        await elementFeature.ChangeStatus(req.params.id, 'archived')
    );
});
router.delete('/block/:id/', async (req: Request, res: Response) => {
    const elementFeature = new ElementFeature(String(req.query.lang));
    res.send(
        await elementFeature.ChangeStatus(req.params.id, 'blocked')
    );
});
router.post('/restore/:id/', async (req: Request, res: Response) => {
    const elementFeature = new ElementFeature(String(req.query.lang));
    res.send(
        await elementFeature.ChangeStatus(req.params.id, 'visible')
    );
});

export default router;