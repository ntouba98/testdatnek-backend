import {
    Op
} from 'sequelize';
import { NotificationFinalStruct } from "../../main/interfaces/struct.interface";
import { dateFormat2, status } from "../../main/scripts/config";
import { CRUD } from "../../main/scripts/crud";
import { DateToString, GetCurrentDate, StringToDate, StringToDate2 } from "../../main/scripts/date";
import { MapAll, RemovePointer } from "../../main/scripts/json";
import { Lower, RandomF, SearchForFind } from "../../main/scripts/string";
import * as InitModels from '../../main/models/init-models';
import { LangForm } from "../forms/lang.form";
import { LangInterface, LangInterface_update } from "../interfaces/lang.interface";
import { models } from '../../main/scripts/bd';


export default class LangFeature extends CRUD {
    nullablePropertiesForEdit: string[] = [];
    nullablePropertiesForUpdate: string[] = [];
    findAll_orderBy: any = {
        code_desc: {
            value: 'code'
            , meaning: 'desc'
        }
        , code_asc: {
            value: 'code'
            , meaning: 'asc'
        }
        , lang_asc: {
            value: 'lang'
            , meaning: 'asc'
        }
        , lang_desc: {
            value: 'lang'
            , meaning: 'desc'
        }
        , spoken_level_asc: {
            value: 'spoken_level'
            , meaning: 'asc'
        }
        , spoken_level_desc: {
            value: 'spoken_level'
            , meaning: 'desc'
        }
        , written_level_asc: {
            value: 'written_level'
            , meaning: 'asc'
        }
        , written_level_desc: {
            value: 'written_level'
            , meaning: 'desc'
        }
        , understanding_level_asc: {
            value: 'understanding_level'
            , meaning: 'asc'
        }
        , understanding_level_desc: {
            value: 'understanding_level'
            , meaning: 'desc'
        }
        , date_added_asc: {
            value: 'date_added'
            , meaning: 'asc'
        }
        , date_added_desc: {
            value: 'date_added'
            , meaning: 'desc'
        }
    };

    notifications:any = {
        good_restored: {
            type: 'success',
            messages: {
                fr: 'la langue a été restaurée avec succès',
                en: 'Lang has been successfully restored',
                nl: 'taal is succesvol hersteld',
            },
        },
        good_blocked: {
            type: 'success',
            messages: {
                fr: 'la langue a été bloquée avec succès',
                en: 'the lang was blocked successfully',
                nl: 'taal is succesvol geblokkeerd',
            },
        },
        good_archived: {
            type: 'success',
            messages: {
                fr: 'la langue a été archivée avec succès',
                en: 'the lang was archived successfully',
                nl: 'de taal is succesvol gearchiveerd',
            },
        },
        good_add: {
            type: 'success',
            messages: {
                fr: 'création de la langue réussi avec succès',
                en: 'successful permission creation',
                nl: 'succesvolle taalcreatie',
            },
        },
        good_update: {
            type: 'success',
            messages: {
                fr: 'Modification de la langue réussi avec succès',
                en: 'Lang modification successfully completed',
                nl: 'Taalverandering succesvol voltooid',
            },
        },
        good_edit: {
            type: 'success',
            messages: {
                fr: 'Edition de la langue réussi avec succès',
                en: 'Edit lang successfully completed',
                nl: 'Taalbewerking succesvol voltooid',
            },
        },
        invalid_data: {
            type: 'warning',
            messages: {
                fr: 'la donnée est invalide',
                en: 'the data is invalid',
                nl: 'de gegevens zijn ongeldig',
            },
        },
        not_found_data: {
            type: 'danger',
            messages: {
                fr: 'la donnée n\'existe pas',
                en: 'the data does not exist',
                nl: 'de gegevens bestaan ​​niet',
            },
        },
        found_data: {
            type: 'danger',
            messages: {
                fr: 'la donnée existe déjà',
                en: 'the data exists',
                nl: 'de gegevens bestaan ​​al',
            },
        },
        unknown_error: {
            type: 'danger',
            messages: {
                fr: 'erreur inconnue , interne et signalé qui sera resolue dans les plus brefs delais',
                en: 'unknown, internal and reported error which will be resolved as soon as possible',
                nl: 'onbekende, interne en gemelde fout die zo snel mogelijk zal worden opgelost',
            },
        },
    };

    public constructor(lang: string = 'fr') {
        super(lang);
    }
    
    public FindAllParams_status(param: any, simplified: boolean = true):any {
        // console.log('----> FindAllParams_status > param:: ', param);
        let val_string = (
            ['visible', 'archived', 'blocked'].includes(param)
        ) ? String(param) : undefined;
        const val_boolean = (Boolean(param)) ? Boolean(param) : undefined;
        const val_int = (parseInt(param)) ? parseInt(param) : undefined;
        const val_float = (parseFloat(param)) ? parseFloat(param) : undefined;

        let vals_string: string[] = [];
        if(
            val_string
        ) {
            vals_string = (simplified) ? [val_string] : SearchForFind(val_string);
        }
        const vals_date: Date[] = vals_string.filter((value: string) => (
            typeof(value) === 'string'
            && value.length > 0
            && !(StringToDate2(value).toString() === 'Invalid Date')
        )).map((value:string) => {
            return StringToDate2(value);
        });

        // - string
        let cond_string: any[] = [];
        if (vals_string.length > 0) {
            cond_string = [
                ...vals_string.map((value) => ({
                    status: value
                }))
            ];
        }
        // - boolean
        const cond_boolean: any[] = [];
        // - int
        const cond_int: any[] = [];
        // - float
        const cond_float: any[] = [];
        // - date
        let cond_date: any[] = [];
        if (vals_date.length > 0) {
            cond_date = [];
        }

        // console.log('----> FindAllParams_status > vals_string:: ', vals_string);
        // console.log('----> FindAllParams_status > cond_string:: ', cond_string);
        // console.log('----> FindAllParams_status > cond_date:: ', cond_date);
        const res = (
            cond_string.length > 0
            || cond_boolean.length > 0
            || cond_int.length > 0
            || cond_float.length > 0
            || cond_date.length > 0
        ) ? {
            [Op.or]: [
                ...cond_string,
                ...cond_boolean,
                ...cond_int,
                ...cond_float,
                ...cond_date,
            ]
        } : undefined;
        console.log('----> FindAllParams_status > res:: ', res);

        return res;
    }
    public async FindAll(
        params: any
        , page: number
        , pages: string | number
        , orderBy: string
    ){
        const paramsConfig = {
            status: {
                funct: this.FindAllParams_status,
                simplified: true
            },
        };
        return await this._findAll(
            models.test_datnek_lang
            , params, paramsConfig, Op.and, page, pages, orderBy, false
        );
    }
    
    async Exists(id: string) {
        return await this._exists<LangInterface>({
            _id: id
        }, this.getIds, this.findCondStrict, models.test_datnek_lang)
    }
    async Find(id: string) {
        return await this._find<LangInterface>({
            _id: id
        }, this.getIds, this.findCondStrict, models.test_datnek_lang)
    }

    async ChangeStatus(id: string, type: 'visible' | 'archived' | 'blocked' = 'archived') {
        return await this._changeStatus<LangInterface>({
            _id: id
        }, type, this.getIds, this.findCond, models.test_datnek_lang)
    }

    async Add(body: any): Promise<NotificationFinalStruct> {
        return await this._add<
            LangInterface
            , InitModels.test_datnek_langAttributes
            , typeof InitModels.test_datnek_lang
            , any
        >(body, this.initBody, this.getIds, this.findCond, models.test_datnek_lang, this.cleanBody, LangForm);
    }
    async Update(body: any): Promise<NotificationFinalStruct> {
        return await this._update<
            LangInterface_update
            , InitModels.test_datnek_langAttributes
            , typeof InitModels.test_datnek_lang
            , any
        >(body, this.initBody_update, this.getIds_update, this.findCondStrict, models.test_datnek_lang, this.cleanBody, LangForm);
    }
    async Edit(body: any): Promise<NotificationFinalStruct> {
        return await this._edit<
            LangInterface
            , InitModels.test_datnek_langAttributes
            , typeof InitModels.test_datnek_lang
            , any
        >(body, this.initBody, this.getIds_update, this.findCond, models.test_datnek_lang, this.cleanBody, LangForm);
    }
    

    public findCond(ids: any): any {
        return {
            [Op.or]: [
                {
                    code: ids._id || null,
                },
                {
                    lang: ids.lang || null,
                },
            ],
        };
    }
    public findCondStrict(ids: any): any {
        return {
            [Op.or]: [
                {
                    code: ids._id || null,
                },
                {
                    lang: ids.lang || null,
                },
            ],
            [Op.and]: [
                {
                    status: 'visible',
                },
            ],
        };
    }
    public getIds(value: LangInterface) {
        return RemovePointer({
            _id: value._id
            , code: value.code
            , lang: value.lang
            , status: value.status
        });
    }
    public getIds_update(value: LangInterface_update) {
        return RemovePointer({
            _id: value._id
            , code: value.code
            , lang: value.lang
            , spoken_level: value.spoken_level
            , written_level: value.written_level
            , understanding_level: value.understanding_level
        });
    }
    public cleanBody(value: any): InitModels.test_datnek_langAttributes {
        return {
            code: value.code
            , lang: value.lang as ('en' | 'fr' | 'nl')
            , spoken_level: value.spoken_level as ('intermediate' | 'pre_intermediate' | 'current' | 'elementary')
            , written_level: value.written_level as ('intermediate' | 'pre_intermediate' | 'current' | 'elementary')
            , understanding_level: value.understanding_level as ('intermediate' | 'pre_intermediate' | 'current' | 'elementary')
            , date_added: value.date_added
            , date_updated: value.date_updated
            , status: value.status as ("blocked" | "visible" | "archived")
        }; 
    }
    public initBody(value: unknown):LangInterface {
        let res:any;
        if (
            typeof(value) === 'object'
            && Array.isArray(value) === false
        ) {
            res = value;
        } else {
            res = {};
        }

        res = {
            _id: res._id,
            code: (
                typeof(res.lang) === 'string'
                && res.lang.length > 0
            ) ? res.lang : RandomF(10, 'alphabetical', (data:string) => `lang${data}`),
            lang: res.lang,
            spoken_level: res.spoken_level,
            written_level: res.written_level,
            understanding_level: res.understanding_level,
            date_added: DateToString(
                (res.date_added instanceof Date) ? res.date_added : GetCurrentDate()
                , dateFormat2
            ),
            date_updated: DateToString(
                (res.date_updated instanceof Date) ? res.date_updated : GetCurrentDate()
                , dateFormat2
            ),
            status: 'visible',
        };

        res._id = (
            typeof(res._id) === 'string'
            && res._id.length > 0
        ) ? res._id : res.code;

        Object.keys(res).forEach((key:string) => {
            if(
                [undefined, NaN].includes(res[key])
                || (
                    typeof res[key] === 'string'
                    && res[key].length <= 0
                )
            ) {
                delete res[key];
            }
        });
        res = MapAll(res, (data:any) => (typeof(data) === 'string') ? Lower(data) : data);

        return res;
    }
    public initBody_update(value: unknown):LangInterface_update {
        let res:any;
        if (
            typeof(value) === 'object'
            && Array.isArray(value) === false
        ) {
            res = value;
        } else {
            res = {};
        }

        res = {
            _id: res._id,
            code: (
                typeof(res.lang) === 'string'
                && res.lang.length > 0
            ) ? res.lang : RandomF(10, 'alphabetical', (data:string) => `lang${data}`),
            lang: res.lang,
            spoken_level: res.spoken_level,
            written_level: res.written_level,
            understanding_level: res.understanding_level,
            date_added: DateToString(
                (res.date_added instanceof Date) ? res.date_added : GetCurrentDate()
                , dateFormat2
            ),
            date_updated: DateToString(
                (res.date_updated instanceof Date) ? res.date_updated : GetCurrentDate()
                , dateFormat2
            ),
            status: 'visible',
        };

        res._id = (
            typeof(res._id) === 'string'
            && res._id.length > 0
        ) ? res._id : res.code;

        Object.keys(res).forEach((key:string) => {
            if(
                [undefined, NaN].includes(res[key])
                || (
                    typeof res[key] === 'string'
                    && res[key].length <= 0
                )
            ) {
                delete res[key];
            }
        });
        res = MapAll(res, (data:any) => (typeof(data) === 'string') ? Lower(data) : data);

        return res;
    }
}