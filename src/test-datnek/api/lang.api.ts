import Config from '../../config.json';
import * as Axios from 'axios';
import { getHeaderRequest, responseEdit, responseSelect } from '../../main/scripts/http';
import { HttpConfig } from '../../main/interfaces/http.interface';
import { LangInterface } from '../interfaces/lang.interface';

const axios = Axios.default;

export default class LangApi {

    async findAll(configHttp: HttpConfig, lang: string): Promise<any> {
      const stringUrl = 'test-datnek/langs';
      const myUrl = new URL(stringUrl, Config.server.host);
      myUrl.searchParams.append('lang', lang);
      myUrl.searchParams.append('page', String(configHttp.page));
      myUrl.searchParams.append('pages', 'all');
      myUrl.searchParams.append('status', 'visible');
      myUrl.searchParams.append('orderBy', String(configHttp.orderBy));
      return axios.get(
        String(myUrl),
        getHeaderRequest(),
      ).then((response: any) => responseSelect(response))
    }
    async find(id: string, lang: string) {
      const stringUrl = `test-datnek/langs/${id}`;
      console.log('----> find - stringUrl:: ', stringUrl);
      const myUrl = new URL(stringUrl, Config.server.host);
      myUrl.searchParams.append('lang', lang);
      return axios.get(
        String(myUrl),
        getHeaderRequest(),
      ).then((response: any) => responseSelect(response))
    }
    async delete(id: string, lang: string): Promise<any> {
      const stringUrl = `test-datnek/langs/archive/${id}`;
      const myUrl = new URL(stringUrl, Config.server.host);
      myUrl.searchParams.append('lang', lang);
      return axios.delete(
        String(myUrl),
        getHeaderRequest(),
      ).then((response: any) => responseEdit(response));
    }
    async edit(element: LangInterface, lang: string): Promise<any> {
      const stringUrl = `test-datnek/langs/edit`;
      const myUrl = new URL(stringUrl, Config.server.host);
      myUrl.searchParams.append('lang', lang);
      return axios.post(
        String(myUrl),
        element,
        getHeaderRequest(),
      ).then((response: any) => responseEdit(response));
    }
}