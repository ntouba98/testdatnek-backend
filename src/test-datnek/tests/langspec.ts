import 'jasmine';
import Joi from 'joi';
import { StructListForm } from '../../main/forms/struct.form';
import * as InitModels from '../../main/models/init-models';
import { GetValidForm } from '../../main/scripts/forms';
import LangApi from '../api/lang.api';
import { LangForm } from '../forms/lang.form';

const langApi = new LangApi();

describe("Lister toutes les langues", function() {
    it("pas d'échec", function() {
        langApi.findAll({
            page: 1,
            pages: 25
        }, 'fr').then((response: any) => {
            console.log('> response:: ', response);
            expect(
                GetValidForm(
                    StructListForm(LangForm),
                    response
                )
            ).toBe(true);
        }).catch((response: any) => {
            expect(
                GetValidForm(
                    StructListForm(LangForm),
                    response
                )
            ).toBe(true);
        });
    });
});