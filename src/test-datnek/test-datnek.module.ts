import { Module } from './interfaces/struct.interface';
import LangRoutes from './routes/lang.routes';

const testDatnekModule: Module[] = [
    {
        url: '/test-datnek/langs',
        router: LangRoutes,
    },
];
export default testDatnekModule;