import Joi from 'joi';
import { langs, levelsLang, status } from '../../main/scripts/config';
import { ArrayToRegExp, RandomF } from '../../main/scripts/string';

export const LangForm  = {
    _id: Joi.string().min(2).max(30),
    code: Joi.string().alphanum().min(2).max(30).required(),
    lang: Joi.string().regex(
        ArrayToRegExp(langs)
    ).required(),
    spoken_level: Joi.string().regex(
        ArrayToRegExp(levelsLang)
    ).required(),
    written_level: Joi.string().regex(
        ArrayToRegExp(levelsLang)
    ).required(),
    understanding_level: Joi.string().regex(
        ArrayToRegExp(levelsLang)
    ).required(),
    date_added: Joi.date().required(),
    date_updated: Joi.date().required(),
    status: Joi.string().regex(
        ArrayToRegExp(status)
    ).required(),
};
export const LangForm_update  = {

};