
export interface LangInterface {
    _id: string,
    code: string;
    lang: 'en' | 'fr' | 'nl';
    spoken_level: 'intermediate' | 'pre_intermediate' | 'current' | 'elementary';
    written_level: 'intermediate' | 'pre_intermediate' | 'current' | 'elementary';
    understanding_level: 'intermediate' | 'pre_intermediate' | 'current' | 'elementary';
    date_added: Date;
    date_updated: Date;
    status: 'visible' | 'archived' | 'blocked';
};
export interface LangInterface_update {
    _id: string,
    code: string;
    lang?: 'en' | 'fr' | 'nl';
    spoken_level?: 'intermediate' | 'pre_intermediate' | 'current' | 'elementary';
    written_level?: 'intermediate' | 'pre_intermediate' | 'current' | 'elementary';
    understanding_level?: 'intermediate' | 'pre_intermediate' | 'current' | 'elementary';
    date_added: Date;
    date_updated: Date;
    status: 'visible' | 'archived' | 'blocked';
};