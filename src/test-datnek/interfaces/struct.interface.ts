import { Router } from 'express';

export interface Main {
    url: string,
    router: Router
};
export interface Module {
    url: string,
    router: Router
};