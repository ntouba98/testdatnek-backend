import express from 'express';
import cors from 'cors';
import morgan from 'morgan';
// import bodyParser from 'body-parser';
import helmet from 'helmet';
import session from 'express-session';
// import cookieSession from 'cookie-session';
import cookieParser from 'cookie-parser';
import csrf from 'csurf';
import path from 'path';
import config from './config.json';
import mainModule from './main/main';
import { RandomF } from './main/scripts/string';

const app = express();
const staticRep = config.server.static_rep;
const port = config.server.port;
const csrfProtection = csrf({ cookie: true });

app.set('view options', {
    layout: false,
});
app.set('trust proxy', 1);
app.disable('x-powered-by');
app.use(express.json());
app.use(cookieParser());
// app.use(csrfProtection)
app.use(morgan('combined'));
app.use(cors());
app.use(helmet({
    contentSecurityPolicy: false,
}));
app.use(
    session({
        secret: RandomF(10, 'alphanumeric'),
        resave: false,
        saveUninitialized: true,
        cookie: { secure: true },
    }),
);
app.set('port', port);

app.use('/', express.static(staticRep));
mainModule.forEach((value: { url: string; router: any; }, index: number) => {
    app.use(value.url, value.router);
})

app.listen(port, () => console.log(`App listening at http://localhost:${port}`));